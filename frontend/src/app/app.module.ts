/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { CoreModule } from './@core/core.module';
import { ThemeModule } from './@theme/theme.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbCardModule,
} from '@nebular/theme';
import {
  NbPasswordAuthStrategy,
  NbAuthModule,
  NbAuthOAuth2JWTToken,
  NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
} from '@nebular/auth';
import { IspitService } from './services/data/ispit.service';
import {ModulService} from './services/data/modul.service';
import {SmerService} from './services/data/smer.service';
import {IspitniRokService} from './services/data/ispitni_rok.service';
import {LokacijeService} from './services/data/lokacije.service';
import {PodesavanjaModule} from './pages/podesavanja/podesavanja.module';
import { AuthGuard } from './services/auth/auth-guard.service';
import {NgxAuthJWTInterceptor} from './services/auth/intercepter';
import {NotLoggedInGuard} from './services/auth/not-logged-in.service';
import {ReactiveFormsModule} from '@angular/forms';
import {PretplataService} from './services/data/pretplata.service';
import {FilterService} from './services/data/filter.service';
import {NbDateFnsDateModule} from '@nebular/date-fns';


@NgModule({
  declarations: [AppComponent],
  imports: [
    PodesavanjaModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbCardModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbToastrModule.forRoot(),
    CoreModule.forRoot(),
    ThemeModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDateFnsDateModule.forRoot({ format: 'dd.MM.yyyy' }),
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',

          token: {
            class: NbAuthOAuth2JWTToken,
            key: 'token',
          },

          baseEndpoint: '/api/v1', // ng serve --proxy-config proxy.conf.json

          login: {
            endpoint: '/login/',
            method: 'post',
          },
          register: {
            endpoint: '/register/',
            method: 'post',
          },
          logout: {
            endpoint: '', // Ovim brisemo token
            method: 'post',
          },

          refreshToken: {
            endpoint: '/token/refresh/',
            method: 'post',
          },

          errors: {
            getter: (module, res, options) => {
              return [Object.entries(res.error)];
            },
          },
        }),
      ],
      forms: {},
    }),
  ],
  providers: [IspitService, ModulService, SmerService, IspitniRokService,
    LokacijeService, PretplataService, FilterService,
    { provide: HTTP_INTERCEPTORS, useClass: NgxAuthJWTInterceptor, multi: true },
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER, useValue: (req) => {
        return ['login', 'logout', 'refresh', 'token'].some(function (v) {
          return req.url.indexOf(v) >= 0;
        });
      },
    },
    AuthGuard,
    NotLoggedInGuard,
  ],

  bootstrap: [AppComponent],
})
export class AppModule {
}
