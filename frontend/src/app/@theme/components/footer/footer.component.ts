import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">

    </span>

        <a href="http://www.matf.bg.ac.rs/" target="_blank">Универзитет у Београду - Математички Факултет</a>
  `,
})
export class FooterComponent {
}
