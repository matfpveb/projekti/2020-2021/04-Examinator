import * as moment from "moment";

export default class DateHelpers {
  static Dani = ['недеља', 'понедењак', 'уторак', 'среда', 'четвртак', 'петак', 'субота'];

  static formatDate(datum: Date) {
    return DateHelpers.Dani[datum.getDay()] + ', ' + [datum.getDate(), datum.getMonth() + 1].join('.') + '.';
  }

  static formatTime(vreme: string) {
    return vreme.split(':').slice(0, 2).join(':');
  }

  static createSQLString(datum: string) {
    return moment(datum).format('YYYY-MM-DD');
  }
}

