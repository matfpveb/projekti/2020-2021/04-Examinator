import {CanActivate, Router} from "@angular/router";
import {NbAuthService} from "@nebular/auth";
import {tap} from "rxjs/operators";
import {Injectable} from "@angular/core";

@Injectable()
export class NotLoggedInGuard implements CanActivate {
  constructor(private authService: NbAuthService, private router: Router) {

  }


  canActivate() {
    return this.authService.isAuthenticated()
      .pipe(
        tap(authenticated => {
          if (authenticated) {
            console.log(authenticated);
            this.router.navigate(['auth/success']);
          }
        }),
      );
  }
}
