import { Inject, Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { NbAuthToken } from '@nebular/auth';
import { NbAuthService } from '@nebular/auth';
import { NB_AUTH_TOKEN_INTERCEPTOR_FILTER } from '@nebular/auth';

@Injectable()
export class NgxAuthJWTInterceptor implements HttpInterceptor {

  constructor(private injector: Injector,
              @Inject(NB_AUTH_TOKEN_INTERCEPTOR_FILTER) protected filter) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.filter(req)) {
      return this.authService.isAuthenticated()
        .pipe(
          switchMap(authenticated => {
              return this.authService.getToken().pipe(
                switchMap((token: NbAuthToken) => {
                  if (token.isValid()) {
                    // The user is logged in and the token is valid - add it to auth header
                    return this.addHeader(token, req, next);
                  } else if ( token.getPayload() === null) {
                    // The token returned is an empty token - the user has not logged in
                    // Request is sent to server without authentication so that the client code
                    // receives the 401/403 error and can act as desired ('session expired', redirect to login, aso)
                    return next.handle(req);
                  } else {
                    // The token is an expired token - we should refresh it and use the new token for auth
                    return this.authService.refreshToken('email', token).pipe(
                      switchMap((t) => { token = t.getToken(); return this.addHeader(token, req, next); } ));
                  }
                }),
              );

          }),
        );
    } else {
      return next.handle(req);
    }
  }

  protected get authService(): NbAuthService {
    return this.injector.get(NbAuthService);
  }

  protected addHeader(token: NbAuthToken, req: HttpRequest<any>, next: HttpHandler) {
    const JWT = `Bearer ${token.getValue()}`;
    req = req.clone({
      setHeaders: {
        Authorization: JWT,
      },
    });
    return next.handle(req);
  }

}

