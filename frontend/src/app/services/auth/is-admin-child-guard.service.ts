import { Injectable } from '@angular/core';
import {CanActivateChild, Router} from "@angular/router";
import {NbAuthService} from "@nebular/auth";

@Injectable({
  providedIn: 'root',
})
export class IsAdminChildGuardService implements CanActivateChild {

  constructor(private authService: NbAuthService, private router: Router) {
  }

  canActivateChild() {
    return this.authService.getToken().map(x => (x.isValid() && x.getPayload()['is_admin']));
  }
}
