import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pretplata } from '../../models/pretplata.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {catchError, find, map, tap} from 'rxjs/operators';
import { of } from 'rxjs';
import {Filteri} from "../../models/filteri.model";

@Injectable()
export class FilterService {
  constructor(private http: HttpClient) {
    this.refreshPretplate();
  }
  private readonly url = '/api/v1/korisnik/podrazumevani_filteri/';

  private filteri;

  refreshPretplate() {
    this.filteri =  this.http.get(this.url);
    return this.filteri;
  }

  getFilteri(): Observable<Filteri> {
    return this.filteri;
  }

  updateFilteri(filteri: Filteri): any {
    return this.http.post(this.url, filteri);
  }


}
