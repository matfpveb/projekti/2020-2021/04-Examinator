import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {SmerModel} from '../../models/smer.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { map } from 'rxjs/operators';
import {IspitniRokModel} from '../../models/ispitni_rok.model';


@Injectable()
export class IspitniRokService {
  constructor(private http: HttpClient) {}
  private url = '/api/v1/ispitni_rokovi/';


  getIspitniRokovi(): Observable<IspitniRokModel[]> {

    return this.http.get(this.url).pipe(
      map((response: IspitniRokModel[]) => {
          return response;
        },
      ),
    );
  }

  getIspitniRokById(id: number) {
    return this.http.get(this.url + id + '/');
  }

  addIspitniRok(ispitni_rok: IspitniRokModel): any {
    return this.http.post(this.url, ispitni_rok);
  }

  updateIspitniRok(id: number, ispitni_rok: IspitniRokModel): any {
    return this.http.put(this.url + id + '/', ispitni_rok);
  }

  deleteIspitniRok(id: number) {
    return this.http.delete(this.url + id + '/');
  }
}
