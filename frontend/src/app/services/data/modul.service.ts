import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {ModulModel} from '../../models/modul.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {  map } from 'rxjs/operators';

@Injectable()
export class ModulService {
  constructor(private http: HttpClient) {}
  private url = '/api/v1/moduli/';


  getModuli(): Observable<ModulModel[]> {

    return this.http.get(this.url ).pipe(
      map((response: ModulModel[]) => {
          return response;
        },
      ),
    );
  }

  getModulById(id: number) {
    return this.http.get(this.url + id + '/');
  }

  addModul(modul: ModulModel): any {
    return this.http.post(this.url, modul);
  }

  updateModul(id: number, modul: ModulModel): any {
    return this.http.put(this.url + id + '/', modul);
  }

  deleteModul(id: number) {
    return this.http.delete(this.url + id + '/');
  }
}
