import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {SmerModel} from '../../models/smer.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { map } from 'rxjs/operators';

import { catchError} from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable()
export class SmerService {
  constructor(private http: HttpClient) {}
  private url = '/api/v1/smerovi/';


  getSmerovi(): Observable<SmerModel[]> {

    return this.http.get(this.url ).pipe(
      map((response: SmerModel[]) => {
          return response;
        },
      ),
    );
  }

  getSmerById(id: number) {
    return this.http.get(this.url + id + '/');
  }

  addSmer(smer: SmerModel): any {
    return this.http.post(this.url, smer);
  }

  updateSmer(id: number, smer: SmerModel): any {
    return this.http.put(this.url + id + '/', smer);
  }

  deleteSmer(id: number) {
    return this.http.delete(this.url + id + '/');
  }
}
