import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Pretplata } from '../../models/pretplata.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {catchError, find, map, tap} from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class PretplataService {
  constructor(private http: HttpClient) {
    this.refreshPretplate();
  }
  private readonly url = '/api/v1/korisnik/pretplate/';

  private pretplate;

  refreshPretplate() {
    this.pretplate =  this.http.get(this.url);
    return this.pretplate;
  }

  getPretplate() {
    return this.pretplate;
  }

  getPretplataForIspit(ispit_id: number): Observable<Pretplata> {
    return this.pretplate.map((subs: Pretplata[]) => subs.find((sub: Pretplata) => sub.ispit === ispit_id));
  }

  addPretplata(id_ispita: number): any {
    return this.http.post(this.url, {ispit: id_ispita});
  }

  deletePretplata(id_pretplate: number): any {
    return this.http.delete(this.url + id_pretplate + '/');
  }

}
