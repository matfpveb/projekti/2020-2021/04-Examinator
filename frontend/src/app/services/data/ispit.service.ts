import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Ispit } from '../../models/ispit.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class IspitService {
  constructor(private http: HttpClient) {}
  private url = '/api/v1/ispiti/';


  getIspiti(): Observable<Ispit[]> {

    return this.http.get(this.url ).pipe(
      map((response: Ispit[]) => {
          return response;
        },
      ),
    );
  }

  getIspitById(id: number): Observable<Ispit> {
    return this.http.get(this.url + id + '/').pipe(
      map((response: Ispit) => {
          return response;
        },
      ),
    );
  }

  addIspit(ispit: Ispit): any {
    return this.http.post(this.url, ispit);
  }

  updateIspit(id: number, ispit: Ispit): any {
    return this.http.put(this.url + id + '/', ispit);
  }

  deleteIspit(id: number) {
    return this.http.delete(this.url + id + '/');
  }
}
