import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {LokacijaModel} from '../../models/lokacija.model';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';


@Injectable()
export class LokacijeService {
  constructor(private http: HttpClient) {}
  private url = '/api/v1/lokacije/';


  getLokacije(): Observable<LokacijaModel[]> {
    return this.http.get(this.url ).pipe(
      map((response: LokacijaModel[]) => {
          return response;
        },
      ),
    );
  }

  getLokacijaById(id: number) {
    return this.http.get(this.url + id + '/');
  }

  addLokacija(lokacija: LokacijaModel): any {
    return this.http.post(this.url, lokacija);
  }

  updateLokacija(id: number, lokacija: LokacijaModel): any {
    return this.http.put(this.url + id + '/', lokacija);
  }

  deleteLokacija(id: number) {
    return this.http.delete(this.url + id + '/');
  }
}
