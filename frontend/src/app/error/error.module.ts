import {NgModule} from "@angular/core";
import {ErrorComponent} from "./error.component";
import {NbButtonModule, NbCardModule, NbLayoutModule} from "@nebular/theme";

@NgModule({
  imports: [
    NbLayoutModule,
    NbCardModule,
    NbButtonModule,
  ],
  declarations: [ ErrorComponent ],
  exports: [
    ErrorComponent,
  ],
})
export class ErrorModule {}
export { ErrorComponent } from "./error.component";
