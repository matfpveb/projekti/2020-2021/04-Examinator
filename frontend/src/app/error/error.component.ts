import { Component, OnInit } from '@angular/core';
import {NbSpinnerService} from '@nebular/theme';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {

  constructor(private spinnerService: NbSpinnerService,
              private router: Router) { }

  ngOnInit(): void {
    this.spinnerService.load();
  }

  goHome() {
    this.router.navigate(['/']);
  }
}
