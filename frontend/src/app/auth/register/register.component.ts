/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import {
  getDeepFromObject,
  NB_AUTH_OPTIONS, NbAuthJWTToken,
  NbAuthResult,
  NbAuthService,
  NbAuthSocialLink,
  NbRegisterComponent
} from "@nebular/auth";


@Component({
  selector: 'nb-register',
  styleUrls: ['./register.component.scss'],
  templateUrl: './register.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationComponent extends NbRegisterComponent implements OnInit{


  ngOnInit() {
    this.service.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      if (token.isValid()) {
        this.router.navigate(['']);
      }
    });
  }


  register(): void {
    super.register();
  }
}
