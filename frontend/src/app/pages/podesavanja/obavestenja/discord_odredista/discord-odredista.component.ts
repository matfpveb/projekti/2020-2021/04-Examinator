import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-discord-odredista',
  templateUrl: './discord-odredista.component.html',
  styleUrls: ['./discord-odredista.component.scss'],
})
export class DiscordOdredistaComponent implements OnInit {

  @Input()
  hasJoined: Subject<boolean>;

  showInput: boolean = false;
  discord_odredista: DiscordOdrediste[] = [];
  api_url = '/api/v1/korisnik/kanali_pretplate/DSC/odredista/';

  discordForm: FormGroup;

  constructor(private http: HttpClient, private formBuilder: FormBuilder) {
    this.http.get(this.api_url).subscribe((odredista: DiscordOdrediste[]) => this.discord_odredista = odredista);
    this.discordForm = this.formBuilder.group({
      discord_username: ['', [Validators.required, Validators.maxLength(32), Validators.minLength(2)]],
      discord_discriminator: ['', [Validators.required, Validators.pattern('^[0-9]{4}$')]],
    });
  }

  ngOnInit(): void {
    this.hasJoined.subscribe(event => this.showInput = true);
  }

  deleteOdrediste(id: number) {
    this.http.delete(this.api_url + id + '/').subscribe(
      success => this.discord_odredista = this.discord_odredista.filter(x => x.id !== id),
    );
  }


  onSubmit(value: any) {
    this.http.post(this.api_url, value)
      .subscribe((d: DiscordOdrediste) => {this.discord_odredista.push(d); this.showInput = false; });
  }

  public get discord_username() {
    return this.discordForm.get('discord_username');
  }

  public get discord_discriminator() {
    return this.discordForm.get('discord_discriminator');
  }
}

class DiscordOdrediste {
  id: number;
  discord_username: string;
  discord_discriminator: string;
}
