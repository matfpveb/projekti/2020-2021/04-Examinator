import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs';

@Component({
  selector: 'ngx-obavestenja',
  templateUrl: './obavestenja.component.html',
  styleUrls: ['./obavestenja.component.scss'],
})
export class ObavestenjaComponent implements OnInit {
  hasJoined: Subject<boolean> = new Subject<boolean>();

  email: boolean;
  discord: boolean;

  base_kanal_api = '/api/v1/korisnik/kanali_pretplate/';
  email_kanal_api = this.base_kanal_api + 'EML/';
  discord_kanal_api = this.base_kanal_api + 'DSC/';

  toggleEmail(checked) {
    this.http.post<Aktivan>(this.email_kanal_api, {'aktivan': checked})
      .subscribe(res => {}, err => this.email = !checked);
  }

  toggleDiscord(checked) {
    this.http.post<Aktivan>(this.discord_kanal_api, {'aktivan': checked})
      .subscribe(res => {}, err => this.discord = !checked);
  }

  discord_link: string;

  constructor(private http: HttpClient) {
    http.get<Link>('/api/v1/discord_link/').subscribe(res => {this.discord_link = res.link; });
    http.get<Aktivan>(this.email_kanal_api).subscribe(res => this.email = res.aktivan);
    http.get<Aktivan>(this.discord_kanal_api).subscribe(res => this.discord = res.aktivan);
  }

  ngOnInit(): void {
  }

  onJoin() {
    this.hasJoined.next(true);
  }

}

class Link {
  link: string;
}

class Aktivan {
  aktivan: boolean;
}
