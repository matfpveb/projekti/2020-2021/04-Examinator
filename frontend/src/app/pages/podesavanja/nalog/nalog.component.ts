import {Component, Inject, OnInit} from '@angular/core';
import {getDeepFromObject, NB_AUTH_OPTIONS} from "@nebular/auth";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'ngx-nalog',
  templateUrl: './nalog.component.html',
  styleUrls: ['./nalog.component.scss'],
})
export class NalogComponent implements OnInit {
  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  lozinka: any = {};

  constructor(private http: HttpClient, @Inject(NB_AUTH_OPTIONS) protected options = {}) { }

  ngOnInit(): void {
  }

  changePassword() {
    this.errors = [];
    this.messages = [];
    // this.submitted = true;

    const apiPoint: string = '/api/v1/korisnik/promena_lozinke/';

    this.http.post<any>(apiPoint, this.lozinka).subscribe(
      res => {
        this.messages.push(' ');
      },
      // Errors will call this callback instead:
      err => {
        this.errors = Object.values(err.error);
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }

}
