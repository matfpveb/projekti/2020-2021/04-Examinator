import {NgModule} from "@angular/core";
import {PodesavanjaComponent} from "./podesavanja/podesavanja.component";
import {ObavestenjaComponent} from "./obavestenja/obavestenja.component";
import {NalogComponent} from "./nalog/nalog.component";
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule, NbIconModule,
  NbInputModule, NbLayoutModule,
  NbSelectModule,
  NbTabsetModule,
  NbThemeService,
  NbToggleModule
} from "@nebular/theme";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PrikazComponent} from "./prikaz/prikaz.component";
import {CommonModule} from "@angular/common";
import {DiscordOdredistaComponent} from "./obavestenja/discord_odredista/discord-odredista.component";

@NgModule({
  imports: [
    NbTabsetModule,
    NbToggleModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    FormsModule,
    CommonModule,
    NbLayoutModule,
    NbButtonModule,
    NbAlertModule,
    NbIconModule,
    ReactiveFormsModule,
  ],
  declarations: [PodesavanjaComponent, ObavestenjaComponent, NalogComponent, PrikazComponent,
                 DiscordOdredistaComponent],
  exports: [
    PodesavanjaComponent,
  ],
})
export class PodesavanjaModule {}
export { PodesavanjaComponent } from "./podesavanja/podesavanja.component";
