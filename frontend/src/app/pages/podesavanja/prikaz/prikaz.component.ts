import { Component, OnInit } from '@angular/core';
import {NbThemeService} from '@nebular/theme';
import {SmerService} from '../../../services/data/smer.service';
import {SmerModel} from '../../../models/smer.model';
import {HttpClient} from '@angular/common/http';
import {ModulService} from '../../../services/data/modul.service';
import {ModulModel} from '../../../models/modul.model';
import {FilterService} from "../../../services/data/filter.service";
import {Filteri} from "../../../models/filteri.model";

@Component({
  selector: 'ngx-prikaz',
  templateUrl: './prikaz.component.html',
  styleUrls: ['./prikaz.component.scss'],
})
export class PrikazComponent implements OnInit {

  smerovi: SmerModel[] = [];
  moduli: ModulModel[] = [];
  selectedSmer;
  selectedModul;

  constructor(private themeService: NbThemeService,
              private smerService: SmerService,
              private modulService: ModulService,
              private filterService: FilterService) {
    smerService.getSmerovi().subscribe((smerovi) => {this.smerovi = smerovi; });
    modulService.getModuli().subscribe((moduli) => {this.moduli = moduli; });
    filterService.getFilteri()
      .subscribe((x: Filteri) => {this.selectedSmer = x.smer_id; this.selectedModul = x.modul_id; });
  }

  getDostupniModuli() {
    return this.moduli.filter(m => m.smer === this.selectedSmer);
  }

  onSmerChange(smer) {
    this.selectedModul = null;
    this.onFilterChange();
  }

  onFilterChange() {
    this.filterService.updateFilteri(new Filteri(this.selectedSmer, this.selectedModul)).subscribe(x => console.log(x));
  }

  ngOnInit(): void {
  }

}
