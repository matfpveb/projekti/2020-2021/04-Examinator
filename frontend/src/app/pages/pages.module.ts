import { NgModule } from '@angular/core';
import {
  NbDatepicker,
  NbDatepickerModule,
  NbLayoutColumnComponent,
  NbLayoutComponent,
  NbMenuModule,
} from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import {ScrollingModule} from '@angular/cdk/scrolling';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    NbDatepickerModule,
    ScrollingModule,
  ],
  declarations: [
    PagesComponent,
  ],
  exports: [
    PagesComponent,
  ],
})
export class PagesModule {
}
