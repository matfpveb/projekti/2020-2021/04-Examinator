import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IspitFormaComponentComponent } from './ispit-forma-component/ispit-forma-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {IspitComponent} from './ispit.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [IspitFormaComponentComponent, IspitComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbDatepickerModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    Ng2SmartTableModule,
  ],
})
export class IspitModule { }
export { IspitComponent } from './ispit.component';
export { IspitFormaComponentComponent } from './ispit-forma-component/ispit-forma-component.component';
