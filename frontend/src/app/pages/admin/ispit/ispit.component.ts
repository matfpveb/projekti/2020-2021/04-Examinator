import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {forkJoin, of} from 'rxjs';

import DateHelpers from '../../../helpers/date.helpers';

// Models
import {IspitFormated} from '../../../models/ispit_formated.model';
import {SmerModel} from '../../../models/smer.model';
import {ModulModel} from '../../../models/modul.model';
import {IspitniRokModel} from '../../../models/ispitni_rok.model';
import {LokacijaModel} from '../../../models/lokacija.model';

// Services
import { IspitService } from '../../../services/data/ispit.service';
import {SmerService} from '../../../services/data/smer.service';
import {ModulService} from '../../../services/data/modul.service';
import {IspitniRokService} from '../../../services/data/ispitni_rok.service';
import {LokacijeService} from '../../../services/data/lokacije.service';
import {NbAuthService} from '@nebular/auth';
import {NbToastrService} from "@nebular/theme";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'ngx-admin-ispiti',
  templateUrl: './ispit.component.html',
})
export class IspitComponent implements OnInit {

  smerovi = [];
  moduli = [];
  rokovi = [];
  lokacije = [];

  updatable_settings = {
    mode: 'external',
    actions: {
      position: 'right',
      edit: true,
      delete: true,
      add: false,
      columnTitle: '',
    },
    add: {
      addButtonContent: 'Dodaj',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      naziv: {
        title: 'Naziv Predmeta',
        type: 'string',
      },
      smerovi: {
        title: 'Smerovi',
        valuePrepareFunction: (data: SmerModel[]) => {
          return data.map(smer => smer.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.smerovi,
          },
        },
        filterFunction: (cell?: SmerModel[], search?: string) => {
          return cell
            .map(smer => smer.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      moduli: {
        title: 'Moduli',
        type: 'string',
        valuePrepareFunction: (data: ModulModel[]) => {
          return data.map(modul => modul.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.moduli,
          },
        },
        filterFunction: (cell?: ModulModel[], search?: string) => {
          return cell
            .map(modul => modul.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      godina_studija: {
        title: 'Godina Studija',
        type: 'number',
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: [
              { value: '1', title: '1' },
              { value: '2', title: '2' },
              { value: '3', title: '3' },
              { value: '4', title: '4' },
              { value: '5', title: '5' },
            ],
          },
        },
      },
      rok: {
        title: 'Ispitni rok',
        type: 'string',
        valuePrepareFunction: ((rok: IspitniRokModel) => rok.naziv),
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.rokovi,
          },
        },
        filterFunction: (cell?: IspitniRokModel, search?: string) => {
          return cell.naziv === search;
        },
      },
      lokacije: {
        title: 'Mesto održavanja',
        type: 'string',
        valuePrepareFunction: (data: LokacijaModel[]) => {
          return data.map(lokacija => lokacija.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.lokacije,
          },
        },
        filterFunction: (cell?: LokacijaModel[], search?: string) => {
          return cell
            .map(lokacija => lokacija.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      datum: {
        title: 'Datum',
        valuePrepareFunction: ((datum: Date) => DateHelpers.formatDate(datum)),
        filterFunction: (cell?: any, search?: string) => {
          return DateHelpers.formatDate(cell).includes(search.trim(), 0);
        },
      },
      vreme: {
        title: 'Vreme',
        valuePrepareFunction: ((vreme: string) => DateHelpers.formatTime(vreme)),
        filterFunction: (cell?: string, search?: string) => {
          return cell.startsWith(search.trim(), 0);
        },
      },
    },
  };

  settings = Object.assign({}, this.updatable_settings);
  source: LocalDataSource;
  errorMessage: string;
  message: string;
  toast_position = 'bottom-end';

  constructor(private ispitService: IspitService,
              private smerService: SmerService,
              private modulService: ModulService,
              private ispitniRokService: IspitniRokService,
              private lokacijeService: LokacijeService,
              private authService: NbAuthService,
              private toastrService: NbToastrService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
    this.source = new LocalDataSource();

    forkJoin([
      ispitService.getIspiti(),
      smerService.getSmerovi(),
      modulService.getModuli(),
      ispitniRokService.getIspitniRokovi(),
      lokacijeService.getLokacije(),
    ]).subscribe(payload => {
      const smerovi = payload[1];
      const moduli = payload[2];
      const rokovi = payload[3];
      const lokacije = payload[4];

      this.insertSmerFilter(smerovi);
      this.insertModulFilter(moduli);
      this.insertRokFilter(rokovi);
      this.insertLokacijaFilter(lokacije);
      const result = payload[0].map(
        ispit => {
          return new IspitFormated().combineIspit(ispit, payload[1], payload[2], payload[3], payload[4]);
        });

      this.source.load(result);
      // Žali Bože hacka... Neophodno je da se kopira ceo objekat u settings da bi se updateovao
      this.settings = Object.assign({}, this.updatable_settings);
    }, error => of(undefined));

    this.source.setSort([{ field: 'godina_studija', direction: 'asc' }]);

    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras?.state as {data: string};
    this.message = state?.data;

  }

  ngOnInit() {
    if(this.message)
      this.showToast(this.toast_position, 'success', this.message);
  }


  insertSmerFilter(res: SmerModel[]) {
    res.forEach((x) => {this.smerovi.push({title: x.naziv, value: x.skracenica }); });
  }

  insertModulFilter(res: ModulModel[]) {
    res.forEach((x) => {this.moduli.push({title: x.naziv, value: x.skracenica }); });
  }

  insertRokFilter(res: IspitniRokModel[]) {
    res.forEach((x) => {this.rokovi.push({title: x.naziv, value: x.naziv }); });
  }

  insertLokacijaFilter(res: LokacijaModel[]) {
    res.forEach((x) => {this.lokacije.push({title: x.lokacija, value: x.skracenica }); });
  }

  showToast(position, status, message) {
    this.toastrService.show(
      '',
      message,
      { position, status });
  }

  onEdit(event) {
    const id = event.data.id;
    this.router.navigate(['izmeni/' + id], { relativeTo: this.route});
  }

  onDelete(row): void {
    const id = row.data.id;
    const naziv = row.data.naziv;
    if (window.confirm('Da li sigurno želite da obrišete ' + naziv + '?')) {
      this.ispitService.deleteIspit(id).subscribe(
        x => {
          this.showToast(this.toast_position, 'success', 'Uspešno brisanje: ' + naziv);
          this.source.remove(row.data);
        }, err => this.showToast(this.toast_position, 'danger', 'Brisanje nije uspelo!'),
      );
    }
  }

  goToDodaj() {
    this.router.navigate(['dodaj'], {relativeTo: this.route});
  }

}
