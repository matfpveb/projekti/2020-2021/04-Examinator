import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SmerService} from '../../../../services/data/smer.service';
import {ModulService} from '../../../../services/data/modul.service';
import {IspitniRokService} from '../../../../services/data/ispitni_rok.service';
import {LokacijeService} from '../../../../services/data/lokacije.service';
import {SmerModel} from '../../../../models/smer.model';
import {ModulModel} from '../../../../models/modul.model';
import {LokacijaModel} from '../../../../models/lokacija.model';
import {IspitniRokModel} from '../../../../models/ispitni_rok.model';
import {forkJoin, of} from 'rxjs';
import {Ispit} from '../../../../models/ispit.model';
import {IspitService} from '../../../../services/data/ispit.service';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import DateHelpers from "../../../../helpers/date.helpers";

@Component({
  selector: 'ngx-ispit-forma-component',
  templateUrl: './ispit-forma-component.component.html',
})
export class IspitFormaComponentComponent implements OnInit {

  buttonText = 'Dodaj ispit';
  ispitForm: FormGroup;
  id: number | undefined;

  public naziv: string;
  public smerovi: number[] = [];
  public moduli: number[] = [];
  public godina_studija: number;
  public rok: number;
  public lokacije: number[] = [];
  public datum: string;
  public vreme: string;

  izbor_smerovi: SmerModel[] = [];
  izbor_moduli: ModulModel[] = [];
  izbor_lokacije: LokacijaModel[] = [];
  izbor_rokovi: IspitniRokModel[] = [];

  constructor(private formBuilder: FormBuilder,
              private smerService: SmerService,
              private modulService: ModulService,
              private rokService: IspitniRokService,
              private lokacijaService: LokacijeService,
              private ispitService: IspitService,
              private route: ActivatedRoute,
              private router: Router,
  ) {
    this.ispitForm = this.formBuilder.group({
      naziv: [this.naziv, [Validators.required]],
      smerovi: [this.smerovi, [Validators.required]],
      moduli: [this.moduli, []],
      godina_studija: [this.godina_studija, [Validators.required]],
      rok: [this.rok, [Validators.required]],
      lokacije: [this.lokacije, [Validators.required]],
      datum: [this.datum, [Validators.required]],
      vreme: [this.vreme, [Validators.required]],
    });

    forkJoin([
      smerService.getSmerovi(),
      modulService.getModuli(),
      rokService.getIspitniRokovi(),
      lokacijaService.getLokacije(),
    ]).subscribe(payload => {
      this.izbor_smerovi = payload[0];
      this.izbor_moduli = payload[1];
      this.izbor_rokovi = payload[2];
      this.izbor_lokacije = payload[3];
    }, error => console.log(error));
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      p => {
        const id = p.get('id');
        if ( id !== null) {
          this.ispitService.getIspitById(parseInt(id, 10))
            .subscribe(
              ispit => this.loadIsipit(ispit),
              err => this.router.navigate(['/404']),
              );
        }
      },err => console.log(err),
    );
  }

  onSubmit(value: any) {
    if (value.moduli.length === 0) {
      value.moduli = this.getDostupniModuli().map(m => m.id);
    }
    const ispit = new Ispit(this.id, value.naziv, value.smerovi, value.moduli, value.godina_studija,
      value.rok, value.lokacije, DateHelpers.createSQLString(value.datum), value.vreme);

    if (this.id !== undefined) {
      this.ispitService.updateIspit(this.id, ispit).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route, state: {data: 'Ispit ' + ispit.naziv + ' uspešno izmenjen!'}};
        this.router.navigate(['../..'], navigationExtras);
      }, err => alert('Izmena ispita nije uspela!'));
    } else {
      this.ispitService.addIspit(ispit).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route, state: {data: 'Ispit ' + ispit.naziv + ' uspešno dodat!'}};
        this.router.navigate(['../'], navigationExtras);
      }, err => alert('Dodavanje ispita nije uspelo!'));
    }
  }

  getDostupniModuli() {
    const izabrani_smerovi: number[] = this.ispitForm.get('smerovi').value;
    return this.izbor_moduli.filter(m => izabrani_smerovi.find(x => x === m.smer));
  }

  loadIsipit(ispit: Ispit) {
    this.id = ispit.id;
    this.ispitForm.setValue({
      naziv: ispit.naziv,
      smerovi: ispit.smerovi,
      moduli: ispit.moduli,
      godina_studija: ispit.godina_studija,
      rok: ispit.rok,
      lokacije: ispit.lokacije,
      datum: new Date(ispit.datum),
      vreme: ispit.vreme,
    });
    this.buttonText = 'Izmeni ispit';
  }
}
