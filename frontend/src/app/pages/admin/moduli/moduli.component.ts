import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

// Services

import {NbAuthService} from '@nebular/auth';
import {NbToastrService} from '@nebular/theme';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {ModulService} from '../../../services/data/modul.service'
import {SmerModel} from "../../../models/smer.model";
import {SmerService} from "../../../services/data/smer.service";
import {forkJoin} from "rxjs";
import {ModulModel} from "../../../models/modul.model";

@Component({
  selector: 'ngx-admin-ispiti',
  templateUrl: './moduli.component.html',
})
export class ModuliComponent implements OnInit {

  smerovi = [];
  smerovi_info: SmerModel[] = [];

  updatable_settings = {
    mode: 'external',
    actions: {
      position: 'right',
      edit: true,
      delete: true,
      add: false,
      columnTitle: '',
    },
    add: {
      addButtonContent: 'Dodaj',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      naziv: {
        title: 'Naziv',
        type: 'string',
      },
      skracenica: {
        title: 'Skraćenica',
        type: 'string',
      },
      smer: {
        title: 'Smerovi',
        valuePrepareFunction: (data: number) => {
          return this.smerovi_info.find((s: SmerModel) => s.id === data).naziv;
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.smerovi,
          },
        },
        filterFunction: (cell?: number, search?: string) => {
          return cell.toString() === search;
        },
      },
    },
  };

  settings = Object.assign({}, this.updatable_settings);

  source: LocalDataSource;
  errorMessage: string;
  message: string;
  toast_position = 'bottom-end';

  constructor(private modulService: ModulService,
              private smerService: SmerService,
              private authService: NbAuthService,
              private toastrService: NbToastrService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
    this.source = new LocalDataSource();

    forkJoin([
      modulService.getModuli(),
      smerService.getSmerovi(),
    ]).subscribe(
      payload => {
        this.smerovi_info = payload[1];
        this.insertSmerFilter(this.smerovi_info);

        this.source.load(payload[0]);
        this.settings = Object.assign({}, this.updatable_settings);
      },
    ),
    modulService.getModuli().subscribe(result => {

    });
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras?.state as {data: string};
    this.message = state?.data;
  }

  ngOnInit() {
    if(this.message)
      this.showToast(this.toast_position, 'success', this.message);
  }

  showToast(position, status, message) {
    this.toastrService.show(
      '',
      message,
      { position, status });
  }

  onEdit(event) {
    const id = event.data.id;
    this.router.navigate(['izmeni/' + id], { relativeTo: this.route});
  }

  insertSmerFilter(res: SmerModel[]) {
    res.forEach((x) => {this.smerovi.push({title: x.naziv, value: x.id }); });
  }

  onDelete(row): void {
    const id = row.data.id;
    const naziv = row.data.naziv;
    if (window.confirm('Da li sigurno želite da obrišete ' + naziv + '?')) {
      this.modulService.deleteModul(id).subscribe(
        x => {
          this.showToast(this.toast_position, 'success', 'Uspešno brisanje: ' + naziv);
          this.source.remove(row.data);
        }, err => this.showToast(this.toast_position, 'danger', 'Brisanje nije uspelo!'),
      );
    }
  }

  goToDodaj() {
    this.router.navigate(['dodaj'], {relativeTo: this.route});
  }

}
