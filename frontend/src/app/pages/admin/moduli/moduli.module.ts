import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulFormaComponentComponent } from './modul-forma-component/modul-forma-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {ModuliComponent} from './moduli.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [ModulFormaComponentComponent, ModuliComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbDatepickerModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    Ng2SmartTableModule,
  ],
})
export class ModuliModule { }
export { ModuliComponent } from './moduli.component';
export { ModulFormaComponentComponent } from './modul-forma-component/modul-forma-component.component';
