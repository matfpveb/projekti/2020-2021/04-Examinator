import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {ModulModel} from '../../../../models/modul.model';
import {ModulService} from '../../../../services/data/modul.service';
import {SmerModel} from "../../../../models/smer.model";
import {SmerService} from "../../../../services/data/smer.service";

@Component({
  selector: 'ngx-modul-forma-component',
  templateUrl: './modul-forma-component.component.html',
})
export class ModulFormaComponentComponent implements OnInit {

  buttonText = 'Dodaj modul';
  modulForm: FormGroup;
  id: number | undefined;

  smerovi: SmerModel[] = [];

  public naziv: string;
  public skracenica: string;
  public smer_id: number;

  constructor(private formBuilder: FormBuilder,
              private modulService: ModulService,
              private smerService: SmerService,
              private route: ActivatedRoute,
              private router: Router,
  ) {
    this.modulForm = this.formBuilder.group({
      naziv: [this.naziv, [Validators.required]],
      skracenica: [this.skracenica, [Validators.required]],
      smer_id: [this.smer_id, [Validators.required]],
    });

    this.smerService.getSmerovi().subscribe(smerovi => this.smerovi = smerovi);
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      p => {
        const id = p.get('id');
        if ( id !== null) {
          this.modulService.getModulById(parseInt(id, 10))
            .subscribe(
              (modul: ModulModel) => this.loadModul(modul),
              err => this.router.navigate(['/404']),
              );
        }
      },err => console.log(err),
    );
  }

  onSubmit(value: any) {
    const modul = new ModulModel(this.id, value.skracenica, value.naziv, value.smer_id);
    console.log(this.smerovi)
    if (this.id !== undefined) {
      this.modulService.updateModul(this.id, modul)
        .subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Modul ' + modul.naziv + ' uspešno izmenjen!'}};

        this.router.navigate(['../..'], navigationExtras);
      }, err => alert('Izmena modula nije uspela!'));
    } else {
      this.modulService.addModul(modul).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Modul ' + modul.naziv + ' uspešno dodat!'}};

        this.router.navigate(['../'], navigationExtras);
      }, err => alert('Dodavanje modula nije uspelo!'));
    }
  }

  loadModul(modul: ModulModel) {
    this.id = modul.id;
    this.modulForm.setValue({
      naziv: modul.naziv,
      skracenica: modul.skracenica,
      smer_id: modul.smer,
    });
    this.buttonText = 'Izmeni modul';
  }

  provera(event) {
    console.log("ASDKASK")
    console.log(event)
  }
}
