import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {IspitniRokModel} from '../../../../models/ispitni_rok.model';
import {IspitniRokService} from '../../../../services/data/ispitni_rok.service';
import DateHelpers from "../../../../helpers/date.helpers";

@Component({
  selector: 'ngx-rok-forma-component',
  templateUrl: './rok-forma-component.component.html',
})
export class RokFormaComponentComponent implements OnInit {

  buttonText = 'Dodaj rok';
  rokForm: FormGroup;
  id: number | undefined;

  public naziv: string;
  public pocetak: string;

  constructor(private formBuilder: FormBuilder,
              private rokService: IspitniRokService,
              private route: ActivatedRoute,
              private router: Router,
  ) {
    this.rokForm = this.formBuilder.group({
      naziv: [this.naziv, [Validators.required]],
      pocetak: [this.pocetak, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      p => {
        const id = p.get('id');
        if ( id !== null) {
          this.rokService.getIspitniRokById(parseInt(id, 10))
            .subscribe(
              (rok: IspitniRokModel) => this.loadIspitniRok(rok),
              err => this.router.navigate(['/404']),
              );
        }
      },err => console.log(err),
    );
  }

  onSubmit(value: any) {
    const rok = new IspitniRokModel(this.id, value.naziv, DateHelpers.createSQLString(value.pocetak));

    if (this.id !== undefined) {
      this.rokService.updateIspitniRok(this.id, rok)
        .subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Ispitni rok ' + rok.naziv + ' uspešno izmenjen!'}};

        this.router.navigate(['../..'], navigationExtras);
      }, err => alert('Izmena ispita nije uspela!'));
    } else {
      this.rokService.addIspitniRok(rok).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Ispitni rok ' + rok.naziv + ' uspešno dodat!'}};

        this.router.navigate(['../'], navigationExtras);
      }, err => alert('Dodavanje roka nije uspelo!'));
    }
  }

  loadIspitniRok(rok: IspitniRokModel) {
    this.id = rok.id;
    this.rokForm.setValue({
      naziv: rok.naziv,
      pocetak: new Date(rok.pocetak),
    });
    this.buttonText = 'Izmeni rok';
  }
}
