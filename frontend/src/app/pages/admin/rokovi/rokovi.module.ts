import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RokFormaComponentComponent } from './rok-forma-component/rok-forma-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {RokoviComponent} from './rokovi.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [RokFormaComponentComponent, RokoviComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbDatepickerModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    Ng2SmartTableModule,
  ],
})
export class RokoviModule { }
export { RokoviComponent } from './rokovi.component';
export { RokFormaComponentComponent } from './rok-forma-component/rok-forma-component.component';
