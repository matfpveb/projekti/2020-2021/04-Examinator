import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmerFormaComponentComponent } from './smer-forma-component/smer-forma-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {SmeroviComponent} from './smerovi.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [SmerFormaComponentComponent, SmeroviComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbDatepickerModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    Ng2SmartTableModule,
  ],
})
export class SmeroviModule { }
export { SmeroviComponent } from './smerovi.component';
export { SmerFormaComponentComponent } from './smer-forma-component/smer-forma-component.component';
