import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {SmerModel} from '../../../../models/smer.model';
import {SmerService} from '../../../../services/data/smer.service';

@Component({
  selector: 'ngx-smer-forma-component',
  templateUrl: './smer-forma-component.component.html',
})
export class SmerFormaComponentComponent implements OnInit {

  buttonText = 'Dodaj smer';
  smerForm: FormGroup;
  id: number | undefined;

  public naziv: string;
  public skracenica: string;

  constructor(private formBuilder: FormBuilder,
              private smerService: SmerService,
              private route: ActivatedRoute,
              private router: Router,
  ) {
    this.smerForm = this.formBuilder.group({
      naziv: [this.naziv, [Validators.required]],
      skracenica: [this.skracenica, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      p => {
        const id = p.get('id');
        if ( id !== null) {
          this.smerService.getSmerById(parseInt(id, 10))
            .subscribe(
              (smer: SmerModel) => this.loadSmer(smer),
              err => this.router.navigate(['/404']),
              );
        }
      },err => console.log(err),
    );
  }

  onSubmit(value: any) {
    const smer = new SmerModel(this.id, value.skracenica, value.naziv);

    if (this.id !== undefined) {
      this.smerService.updateSmer(this.id, smer)
        .subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Ispitni smer ' + smer.naziv + ' uspešno izmenjen!'}};

        this.router.navigate(['../..'], navigationExtras);
      }, err => alert('Izmena ispita nije uspela!'));
    } else {
      this.smerService.addSmer(smer).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Ispitni smer ' + smer.naziv + ' uspešno dodat!'}};

        this.router.navigate(['../'], navigationExtras);
      }, err => alert('Dodavanje smera nije uspelo!'));
    }
  }

  loadSmer(smer: SmerModel) {
    this.id = smer.id;
    this.smerForm.setValue({
      naziv: smer.naziv,
      skracenica: smer.skracenica,
    });
    this.buttonText = 'Izmeni smer';
  }
}
