import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

// Services

import {LokacijeService} from '../../../services/data/lokacije.service';
import {NbAuthService} from '@nebular/auth';
import {NbToastrService} from "@nebular/theme";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'ngx-admin-ispiti',
  templateUrl: './lokacije.component.html',
})
export class LokacijeComponent implements OnInit {

  smerovi = [];
  moduli = [];
  rokovi = [];
  lokacije = [];

  settings = {
    mode: 'external',
    actions: {
      position: 'right',
      edit: true,
      delete: true,
      add: false,
      columnTitle: '',
    },
    add: {
      addButtonContent: 'Dodaj',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      lokacija: {
        title: 'Adresa',
        type: 'string',
      },
      skracenica: {
        title: 'Skracenica',
        type: 'string',
      },
    },
  };

  source: LocalDataSource;
  errorMessage: string;
  message: string;
  toast_position = 'bottom-end';

  constructor(private lokacijeService: LokacijeService,
              private authService: NbAuthService,
              private toastrService: NbToastrService,
              private router: Router,
              private route: ActivatedRoute,
  ) {
    this.source = new LocalDataSource();

    lokacijeService.getLokacije().subscribe(result => {
      this.source.load(result);
    });
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras?.state as {data: string};
    this.message = state?.data;
  }

  ngOnInit() {
    if(this.message)
      this.showToast(this.toast_position, 'success', this.message);
  }

  showToast(position, status, message) {
    this.toastrService.show(
      '',
      message,
      { position, status });
  }

  onEdit(event) {
    const id = event.data.id;
    this.router.navigate(['izmeni/' + id], { relativeTo: this.route});
  }

  onDelete(row): void {
    const id = row.data.id;
    const naziv = row.data.skracenica;
    if (window.confirm('Da li sigurno želite da obrišete ' + naziv + '?')) {
      this.lokacijeService.deleteLokacija(id).subscribe(
        x => {
          this.showToast(this.toast_position, 'success', 'Uspešno brisanje: ' + naziv);
          this.source.remove(row.data);
        }, err => this.showToast(this.toast_position, 'danger', 'Brisanje nije uspelo!'),
      );
    }
  }

  goToDodaj() {
    this.router.navigate(['dodaj'], {relativeTo: this.route});
  }

}
