import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LokacijeService} from '../../../../services/data/lokacije.service';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {LokacijaModel} from '../../../../models/lokacija.model';

@Component({
  selector: 'ngx-lokacija-forma-component',
  templateUrl: './lokacija-forma-component.component.html',
})
export class LokacijaFormaComponentComponent implements OnInit {

  buttonText = 'Dodaj lokaciju';
  lokacijaForm: FormGroup;
  id: number | undefined;

  public lokacija: string;
  public skracenica: string;

  constructor(private formBuilder: FormBuilder,
              private lokacijaService: LokacijeService,
              private route: ActivatedRoute,
              private router: Router,
  ) {
    this.lokacijaForm = this.formBuilder.group({
      lokacija: [this.lokacija, [Validators.required]],
      skracenica: [this.skracenica, [Validators.required, Validators.maxLength(8)]],
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      p => {
        const id = p.get('id');
        if ( id !== null) {
          this.lokacijaService.getLokacijaById(parseInt(id, 10))
            .subscribe(
              (lokacija: LokacijaModel) => this.loadLokacija(lokacija),
              err => this.router.navigate(['/404']),
              );
        }
      },err => console.log(err),
    );
  }

  onSubmit(value: any) {

    const lokacija = new LokacijaModel(this.id, value.skracenica, value.lokacija);

    if (this.id !== undefined) {
      this.lokacijaService.updateLokacija(this.id, lokacija)
        .subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Lokacija ' + lokacija.skracenica + ' uspešno izmenjena!'}};

        this.router.navigate(['../..'], navigationExtras);
      }, err => alert('Izmena ispita nije uspela!'));
    } else {
      this.lokacijaService.addLokacija(lokacija).subscribe(x => {
        const navigationExtras: NavigationExtras = { relativeTo: this.route,
          state: {data: 'Lokacija ' + lokacija.skracenica + ' uspešno dodata!'}};

        this.router.navigate(['../'], navigationExtras);
      }, err => alert('Dodavanje lokacije nije uspelo!'));
    }
  }

  loadLokacija(lokacija: LokacijaModel) {
    this.id = lokacija.id;
    this.lokacijaForm.setValue({
      lokacija: lokacija.lokacija,
      skracenica: lokacija.skracenica,
    });
    this.buttonText = 'Izmeni lokaciju';
  }
}
