import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LokacijaFormaComponentComponent } from './lokacija-forma-component/lokacija-forma-component.component';
import {ReactiveFormsModule} from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule,
  NbInputModule,
  NbSelectModule,
} from '@nebular/theme';
import {LokacijeComponent} from './lokacije.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  declarations: [LokacijaFormaComponentComponent, LokacijeComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NbCardModule,
    NbDatepickerModule,
    NbSelectModule,
    NbInputModule,
    NbCheckboxModule,
    NbButtonModule,
    Ng2SmartTableModule,
  ],
})
export class LokacijeModule { }
export { LokacijeComponent } from './lokacije.component';
export { LokacijaFormaComponentComponent } from './lokacija-forma-component/lokacija-forma-component.component';
