import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import {PodesavanjaComponent} from './podesavanja/podesavanja.module';
import { AuthGuard } from '../services/auth/auth-guard.service';
import {IspitComponent, IspitFormaComponentComponent} from "./admin/ispit/ispit.module";
import {LokacijaFormaComponentComponent, LokacijeComponent} from "./admin/lokacije/lokacije.module";
import {RokFormaComponentComponent, RokoviComponent} from "./admin/rokovi/rokovi.module";
import {SmerFormaComponentComponent, SmeroviComponent} from "./admin/smerovi/smerovi.module";
import {ModulFormaComponentComponent, ModuliComponent} from "./admin/moduli/moduli.module";
import {IsAdminChildGuardService} from "../services/auth/is-admin-child-guard.service";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'admin',
      canActivateChild: [IsAdminChildGuardService],
      children: [
        {
          path: 'ispiti',
          children: [
            {
              path: 'dodaj',
              component: IspitFormaComponentComponent,
            },
            {
              path: 'izmeni/:id',
              component: IspitFormaComponentComponent,
            },
            {
              path: '',
              component: IspitComponent,
            },
          ],
        },
        {
          path: 'rokovi',
          children: [
            {
              path: 'dodaj',
              component: RokFormaComponentComponent,
            },
            {
              path: 'izmeni/:id',
              component: RokFormaComponentComponent,
            },
            {
              path: '',
              component: RokoviComponent,
            },
          ],
        },
        {
          path: 'lokacije',
          children: [
            {
              path: 'dodaj',
              component: LokacijaFormaComponentComponent,
            },
            {
              path: 'izmeni/:id',
              component: LokacijaFormaComponentComponent,
            },
            {
              path: '',
              component: LokacijeComponent,
            },
          ],
        },
        {
          path: 'smerovi',
          children: [
            {
              path: 'dodaj',
              component: SmerFormaComponentComponent,
            },
            {
              path: 'izmeni/:id',
              component: SmerFormaComponentComponent,
            },
            {
              path: '',
              component: SmeroviComponent,
            },
          ],
        },
        {
          path: 'moduli',
          children: [
            {
              path: 'dodaj',
              component: ModulFormaComponentComponent,
            },
            {
              path: 'izmeni/:id',
              component: ModulFormaComponentComponent,
            },
            {
              path: '',
              component: ModuliComponent,
            },
          ],
        },
      ],
    },
    {
      path: '',
      loadChildren: () => import('./student/table.module')
        .then(m => m.TableModule),
    },
    {
      path: 'podesavanja',
      canActivate: [AuthGuard],
      component: PodesavanjaComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
