import {Component, OnInit} from '@angular/core';
import {NbAuthService} from "@nebular/auth";

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  protected is_admin = false;

  constructor(private authService: NbAuthService) {
    authService.onTokenChange().subscribe(
      token => {
        this.is_admin = token.isValid() && token.getPayload()['is_admin'];
        this.menu = this.constructMenu(this.is_admin);
      },
    );
  }

  ngOnInit() {
    this.menu = this.constructMenu(this.is_admin);
  }

  constructMenu(is_admin: boolean) {
    return [
      {
        title: 'Tabela ispita',
        icon: 'grid-outline',
        link: '/',
        hidden: is_admin,
      },
      {
        title: 'Ispiti',
        icon: 'file-text-outline',
        link: '/admin/ispiti',
        hidden: !is_admin,
      },
      {
        title: 'Rokovi',
        icon: 'calendar-outline',
        link: '/admin/rokovi',
        hidden: !is_admin,
      },
      {
        title: 'Lokacije',
        icon: 'map-outline',
        link: '/admin/lokacije',
        hidden: !is_admin,
      },
      {
        title: 'Smerovi',
        icon: 'swap-outline',
        link: '/admin/smerovi',
        hidden: !is_admin,
      },
      {
        title: 'Moduli',
        icon: 'sync-outline',
        link: '/admin/moduli',
        hidden: !is_admin,
      },
    ];
  }

  menu = [];
}
