import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TableComponent } from './table.component';
import { ExamTableComponent } from './exam-table/component/exam-table.component';

const routes: Routes = [{
  path: '',
  component: TableComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TableRoutingModule { }

export const routedComponents = [
  TableComponent,
  ExamTableComponent,
];
