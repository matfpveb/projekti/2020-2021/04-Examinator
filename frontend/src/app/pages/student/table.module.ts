import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbSpinnerModule,
  NbTreeGridModule
} from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { TableRoutingModule, routedComponents } from './table-routing.module';
import {SubscribeButtonComponent} from "./exam-table/subscribe/subscribe.component";

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    TableRoutingModule,
    Ng2SmartTableModule,
    NbButtonModule,
    NbSpinnerModule,
  ],
  declarations: [
    ...routedComponents, SubscribeButtonComponent,
  ],
})
export class TableModule { }
