import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import {forkJoin, of} from 'rxjs';

import DateHelpers from '../../../../helpers/date.helpers';

// Models
import {IspitFormated} from '../../../../models/ispit_formated.model';
import {SmerModel} from '../../../../models/smer.model';
import {ModulModel} from '../../../../models/modul.model';
import {IspitniRokModel} from '../../../../models/ispitni_rok.model';
import {LokacijaModel} from '../../../../models/lokacija.model';

// Services
import { IspitService } from '../../../../services/data/ispit.service';
import {SmerService} from '../../../../services/data/smer.service';
import {ModulService} from '../../../../services/data/modul.service';
import {IspitniRokService} from '../../../../services/data/ispitni_rok.service';
import {LokacijeService} from '../../../../services/data/lokacije.service';
import {NbAuthService} from '@nebular/auth';
import {SubscribeButtonComponent} from '../subscribe/subscribe.component';
import {FilterService} from '../../../../services/data/filter.service';

@Component({
  selector: 'ngx-exam-table',
  templateUrl: './exam-table.component.html',
  styleUrls: ['./exam-table.component.scss'],
})
export class ExamTableComponent implements OnInit {
  loading = true;

  smerovi = [];
  moduli = [];
  rokovi = [];
  lokacije = [];

  updatable_settings = {
    actions: false,
    columns: {
      naziv: {
        title: 'Naziv Predmeta',
        type: 'string',
      },
      smerovi: {
        title: 'Smerovi',
        valuePrepareFunction: (data: SmerModel[]) => {
          return data.map(smer => smer.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.smerovi,
          },
        },
        filterFunction: (cell?: SmerModel[], search?: string) => {
          return cell
            .map(modul => modul.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      moduli: {
        title: 'Moduli',
        type: 'string',
        valuePrepareFunction: (data: ModulModel[]) => {
          return data.map(modul => modul.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.moduli,
          },
        },
        filterFunction: (cell?: ModulModel[], search?: string) => {
          return cell
            .map(modul => modul.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      godina_studija: {
        title: 'Godina Studija',
        type: 'number',
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: [
              { value: '1', title: '1' },
              { value: '2', title: '2' },
              { value: '3', title: '3' },
              { value: '4', title: '4' },
              { value: '5', title: '5' },
            ],
          },
        },
      },
      rok: {
        title: 'Ispitni rok',
        type: 'string',
        valuePrepareFunction: ((rok: IspitniRokModel) => rok.naziv),
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.rokovi,
          },
        },
        filterFunction: (cell?: IspitniRokModel, search?: string) => {
          return cell.naziv === search;
        },
      },
      lokacije: {
        title: 'Mesto održavanja',
        type: 'string',
        valuePrepareFunction: (data: LokacijaModel[]) => {
          return data.map(lokacija => lokacija.skracenica).join(', ');
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Odaberi...',
            list: this.lokacije,
          },
        },
        filterFunction: (cell?: LokacijaModel[], search?: string) => {
          return cell
            .map(lokacija => lokacija.skracenica === search)
            .reduce((a, b) => a || b, false );
        },
      },
      datum: {
        title: 'Datum',
        valuePrepareFunction: ((datum: Date) => DateHelpers.formatDate(datum)),
        filterFunction: (cell?: any, search?: string) => {
          return DateHelpers.formatDate(cell).includes(search.trim(), 0);
        },
      },
      vreme: {
        title: 'Vreme',
        valuePrepareFunction: ((vreme: string) => DateHelpers.formatTime(vreme)),
        filterFunction: (cell?: string, search?: string) => {
          return cell.startsWith(search.trim(), 0);
        },
      },
      pretplata: {
        title: 'Obaveštenja',
        type: 'custom',
        renderComponent: SubscribeButtonComponent,
        onComponentInitFunction(instance) {
          instance.save.subscribe();
        },
      },
    },
  };

  settings = Object.assign({}, this.updatable_settings);
  source: LocalDataSource;
  errorMessage: string;

  constructor(private ispitService: IspitService,
              private smerService: SmerService,
              private modulService: ModulService,
              private ispitniRokService: IspitniRokService,
              private lokacijeService: LokacijeService,
              private authService: NbAuthService,
              private filterService: FilterService,
              ) {
    this.source = new LocalDataSource();

    forkJoin([
      ispitService.getIspiti(),
      smerService.getSmerovi(),
      modulService.getModuli(),
      ispitniRokService.getIspitniRokovi(),
      lokacijeService.getLokacije(),
      filterService.getFilteri().catch(e => of(undefined)),
    ]).subscribe(payload => {
        const smerovi = payload[1];
        const moduli = payload[2];
        const rokovi = payload[3];
        const lokacije = payload[4];

        this.insertSmerFilter(smerovi);
        this.insertModulFilter(moduli);
        this.insertRokFilter(rokovi);
        this.insertLokacijaFilter(lokacije);
        const result = payload[0].map(
          ispit => {
            return new IspitFormated().combineIspit(ispit, payload[1], payload[2], payload[3], payload[4]);
          });

        this.source.load(result);
        // Žali Bože hacka... Neophodno je da se kopira ceo objekat u settings da bi se updateovao
        this.settings = Object.assign({}, this.updatable_settings);

        const filter = payload[5];
        if (filter !== undefined) {
          const smer = smerovi.find((x: SmerModel) => x.id === filter.smer_id);
          if (smer) {
            this.source.addFilter({ field: 'smerovi', search: smer.skracenica });
          }
          const modul = moduli.find((x: ModulModel) => x.id === filter.modul_id);
          if (modul) {
            this.source.addFilter({ field: 'moduli', search: modul.skracenica });
          }

          this.loading = false;
        }


      }, error => of(undefined));

    this.source.setSort([{ field: 'godina_studija', direction: 'asc' }]);
  }

  ngOnInit() {
    this.hidePretplataColumnForAnon();
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }


  insertSmerFilter(res: SmerModel[]) {
    res.forEach((x) => {this.smerovi.push({title: x.naziv, value: x.skracenica }); });
  }

  insertModulFilter(res: ModulModel[]) {
    res.forEach((x) => {this.moduli.push({title: x.naziv, value: x.skracenica }); });
  }

  insertRokFilter(res: IspitniRokModel[]) {
    res.forEach((x) => {this.rokovi.push({title: x.naziv, value: x.naziv }); });
  }

  insertLokacijaFilter(res: LokacijaModel[]) {
    res.forEach((x) => {this.lokacije.push({title: x.lokacija, value: x.skracenica }); });
  }

  hidePretplataColumnForAnon() {
    this.authService.isAuthenticated()
      .subscribe((isAuth: boolean) => !isAuth && delete this.settings.columns.pretplata);
  }
}
