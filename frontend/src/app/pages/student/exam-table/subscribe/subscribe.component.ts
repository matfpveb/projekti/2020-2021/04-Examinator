import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from 'ng2-smart-table';
import {PretplataService} from "../../../../services/data/pretplata.service";
import {Pretplata} from "../../../../models/pretplata.model";

@Component({
  selector: 'ngx-subscribe-button',
  template: `
    <button nbButton status={{status}} (click)="onClick()">{{ tekst }}</button>
  `,
})
export class SubscribeButtonComponent implements ViewCell, OnInit {
  pretplacen: boolean;
  pretplata_id: number | undefined;
  tekst: string = 'prijavi';
  ispit_id: number;
  status: string = 'success';

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  constructor(private pretplataService: PretplataService, private detector: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.ispit_id = this.rowData.id;
    this.pretplataService.getPretplataForIspit(this.ispit_id)
      .subscribe((p: Pretplata) => this.updateButton(p));
  }

  onClick() {
    if (this.pretplacen) {
      this.pretplataService.deletePretplata(this.pretplata_id).subscribe(p => this.updateButton(undefined),
                                                                           err => console.log('error'));
    } else {
      this.pretplataService.addPretplata(this.ispit_id).subscribe((p: Pretplata) => this.updateButton(p));
    }
  }

  updateButton(p: Pretplata) {
    if (p !== undefined) {
      this.pretplata_id = p.id;
      this.pretplacen = true;
      this.tekst = 'otprati';
      this.status = 'danger';
    } else {
      this.pretplata_id = undefined;
      this.pretplacen = false;
      this.tekst = 'zaprati';
      this.status = 'success';
    }
    this.detector.markForCheck();
  }
}
