import { Component } from '@angular/core';

@Component({
  selector: 'ngx-tables',
  template: `<ngx-exam-table></ngx-exam-table>`,
})
export class TableComponent {
}
