export class ModulModel {
  constructor(public id: number,
              public naziv: string,
              public skracenica: string,
              public smer: number,
              ) { }
}
