export class LokacijaModel {
  constructor(public id: number,
              public skracenica: string,
              public lokacija: string,
              ) { }
}
