import {SmerModel} from './smer.model';
import {Ispit} from './ispit.model';
import {ModulModel} from './modul.model';
import {IspitniRokModel} from './ispitni_rok.model';
import {LokacijaModel} from './lokacija.model';

export class IspitFormated {
  public id: number;
  public naziv: string;
  public smerovi: SmerModel[];
  public moduli: ModulModel[];
  public godina_studija: number;
  public rok: IspitniRokModel;
  public lokacije: LokacijaModel[];
  public datum: Date;
  public vreme: string;

  constructor(
  ) { }

  combineIspit(
    ispit: Ispit,
    smerovi: SmerModel[],
    moduli: ModulModel[],
    rokovi: IspitniRokModel[],
    lokacije: LokacijaModel[],
  ) {
    this.id = ispit.id;
    this.naziv = ispit.naziv;
    this.datum = new Date(ispit.datum);
    this.vreme = ispit.vreme;
    this.godina_studija = ispit.godina_studija;

    this.rok = rokovi.find(rok => rok.id === ispit.rok);
    this.smerovi = smerovi.filter(smer => ispit.smerovi.includes(smer.id));
    this.moduli = moduli.filter(modul => ispit.moduli.includes(modul.id));
    this.lokacije = lokacije.filter(lokacija => ispit.lokacije.includes(lokacija.id));

    return this;
  }
}
