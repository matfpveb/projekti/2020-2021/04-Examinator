export class Ispit {
  constructor(public id: number|undefined,
              public naziv: string,
              public smerovi: number[],
              public moduli: number[],
              public godina_studija: number,
              public rok: number,
              public lokacije: number[],
              public datum: string,
              public vreme: string,
              ) { }
}
