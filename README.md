# Project Examinator

A web platform to help MATF students plan their exams by providing them with a concise view of the exam schedule, as well as advanced search, event subscriptions and info sharing. 

## Developers

- [Konstantin Klima, 476/2018](https://gitlab.com/konstantin-klima)
- [Ana Bolović, 436/2019](https://gitlab.com/anabolovic)
- [Milan Vićić, 354/2020](https://gitlab.com/milanvica)
- [Pavle Veličković, 67/2017](https://gitlab.com/pvelickovic)
