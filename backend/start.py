#!/usr/bin/env python

import sys
import subprocess
from pathlib import Path
from tqdm import tqdm

MANAGE = "manage.py"
ISPITI_DIR = "src/examinator/fixtures/ispiti"

def print_usage_and_exit():
    print(
        '''
Examinator project script - please run with one of the arguments:
    setup - Runs migrations and first time setup
    serve - Runs the Django development server on http://127.0.0.1:8000/
    load  - Cleans DB and populates it with data
    clean - Cleans DB
        '''
        )
    sys.exit(1)

def print_venv_and_exit():
    print(
        '''
Examinator project script - please set source to virtual enviroment first!
        '''
          )
    sys.exit(1)

def setup():
    subprocess.run(["python3", MANAGE, "migrate"])

def serve():
    subprocess.run(["python3", MANAGE, "runserver"])

def clean():
    subprocess.run(["python3", MANAGE, "flush", "--no-input"])

def load():
    clean()
    log = []

    prerequsites = ['ispitni_rokovi', 'smerovi', 'moduli', 'lokacije', 'users']
    for p in prerequsites:
        load_from_file(p, log)

    walk(Path(ISPITI_DIR), log)

    if len(log):
        print(f"Failed to load {len(log)} files")
        files = '\n'.join(log)
        print("Failed to load:")
        print(files)

def load_from_file(file: str, log: list):
    proc = subprocess.run(["python3", MANAGE, "loaddata", file], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if proc.returncode:
        log.append(file)

def walk(root: Path, log: list):
    for f in tqdm(root.iterdir()):
        if f.is_dir():
            walk(f, log)
        else:
            load_from_file(str(f), log)

def main():
    if sys.prefix == sys.base_prefix:
        print_venv_and_exit()

    if len(sys.argv) != 2:
        print_usage_and_exit()

    argument = sys.argv[1]
    if argument == "setup":
        setup()
    elif argument == "clean":
        clean()
    elif argument == "load":
        load()
    elif argument == "serve":
        serve()
    else:
        print_usage_and_exit()

if __name__ == '__main__':
    main()