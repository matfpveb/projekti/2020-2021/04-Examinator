import os
import sys

import django
from django.apps import apps
from django.conf import settings
from celery import Celery

app = Celery('src')
app.config_from_object('django.conf:settings', namespace='CELERY')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'src.settings')
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))
django.setup()

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))