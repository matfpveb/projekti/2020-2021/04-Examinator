from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import User

class Lokacija(models.Model):
    skracenica = models.TextField(max_length=8)
    lokacija = models.TextField(max_length=128)

class IspitniRok(models.Model):
    naziv = models.TextField(max_length=256)
    pocetak = models.DateField()

    class Meta:
        ordering = ['pocetak']

class Smer(models.Model):
    skracenica = models.TextField(max_length=8)
    naziv = models.TextField(max_length=64)

class Modul(models.Model):
    skracenica = models.TextField(max_length=8)
    naziv = models.TextField(max_length=64)
    smer = models.ForeignKey(Smer, on_delete=models.CASCADE)

class Ispit(models.Model):
    naziv = models.TextField(max_length=256)
    smerovi = models.ManyToManyField(Smer)
    moduli = models.ManyToManyField(Modul)
    godina_studija = models.SmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])

    rok = models.ForeignKey(IspitniRok, on_delete=models.CASCADE)

    lokacije = models.ManyToManyField(Lokacija)
    datum = models.DateField()
    vreme = models.TimeField()


class Pretplata(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ispit = models.ForeignKey(Ispit, on_delete=models.CASCADE)

class KanalPretplate(models.Model):
    EMAIL = "EML"
    DISCORD = "DSC"
    PUSH = "PSH"

    CHANNEL_CHOICES = (
        (EMAIL, "email"),
        (DISCORD, "discord"),
        (PUSH, "push")
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tip = models.CharField(max_length=3, choices=CHANNEL_CHOICES, default=EMAIL)
    aktivan = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'tip')

class OdredistePretplate(models.Model):
    kanal_pretplate = models.ForeignKey(KanalPretplate, on_delete=models.CASCADE)

    class Meta:
        abstract = True

class EmailOdrediste(OdredistePretplate):
    email = models.EmailField()

class DiscordOdrediste(OdredistePretplate):
    discord_username = models.CharField(max_length=32)
    discord_discriminator = models.CharField(max_length=4)

class PodrazumevaniFilteri(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    smer = models.ForeignKey(Smer, on_delete=models.SET_NULL, null=True)
    modul = models.ForeignKey(Modul, on_delete=models.SET_NULL, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['user'], name="one-per-user"),
        ]

