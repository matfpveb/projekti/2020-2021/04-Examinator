
from django.http import JsonResponse
from src.examinator.helper import yaml_loader, csv_loader
from django.http import JsonResponse, Http404
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenViewBase

from src.examinator.serializers import *
from src.examinator.models import *
from rest_framework import generics, views, status, mixins, viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny, DjangoModelPermissionsOrAnonReadOnly, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from src.settings import DISCORD_INVITE_LINK


class LokacijaList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Lokacija.objects.all()
    serializer_class = LokacijaSerializer

class LokacijaDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Lokacija.objects.all()
    serializer_class = LokacijaSerializer

class IspitniRokList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = IspitniRok.objects.all()
    serializer_class = IspitniRokSerializer

class IspitniRokDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = IspitniRok.objects.all()
    serializer_class = IspitniRokSerializer

class SmerList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Smer.objects.all()
    serializer_class = SmerSerializer

class SmerDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Smer.objects.all()
    serializer_class = SmerSerializer

class ModulList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Modul.objects.all()
    serializer_class = ModulSerializer

class ModulDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Modul.objects.all()
    serializer_class = ModulSerializer

class IspitList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Ispit.objects.all()
    serializer_class = IspitSerializer

class IspitDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    queryset = Ispit.objects.all()
    serializer_class = IspitSerializer

class IspitByRokList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = IspitSerializer

    def get_queryset(self):
        return Ispit.objects.filter(rok=self.kwargs.get('rok_id'))


class IspitByRokDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, DjangoModelPermissionsOrAnonReadOnly]
    serializer_class = IspitSerializer

    def get_queryset(self):
        return Ispit.objects.filter(rok=self.kwargs.get('rok_id'))


class LogoutView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        print(request.data)
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class RegisterView(generics.CreateAPIView):
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegistrationSerializer

class DiscordLinkView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        return JsonResponse({'link': DISCORD_INVITE_LINK})

class ChangePasswordView(APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        attrs = request.data
        currentPassword: str = attrs.get('currentPassword', "")
        password: str = attrs.get('password', "")
        confirmPassword: str = attrs.get('confirmPassword', "")

        user: User = request.user
        if not user.check_password(currentPassword):
            raise serializers.ValidationError({"currentPassword": "Neispravna trenutna lozinka!"})

        if password != confirmPassword:
            raise serializers.ValidationError({"password": "Lozinke moraju biti iste"})

        user.set_password(password)
        user.save()

        return Response(status=status.HTTP_200_OK)


class PretplataViewList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PretplataSerializer

    def get_queryset(self):
        return Pretplata.objects.filter(user=self.request.user)

class PretplataViewDetail(generics.RetrieveDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = PretplataSerializer

    def get_queryset(self):
        return Pretplata.objects.filter(user=self.request.user)


def parseYaml(filePath):
    data_all = yaml_loader(filePath)
    error = ""
    for data in data_all:
        try:
            ispit = Ispit()
            ispit.naziv = data.get('naziv')
            ispit.godina_studija = data.get('godina_studija')
            ispit.datum = data.get('datum')
            ispit.vreme = data.get('vreme')

            rok_za_dodavanje = IspitniRok.objects.get(naziv = data['rok'])
            ispit.rok = rok_za_dodavanje
            
            smerovi_za_dodavanje = Smer.objects.filter(skracenica__in = data['smerovi']).all()
            for s in smerovi_za_dodavanje:
                ispit.smerovi.add(s)

            lokacije_za_dodavanje = Lokacija.objects.filter(skracenica__in = data['lokacije']).all()
            for l in lokacije_za_dodavanje:
                ispit.lokacije.add(l)

            moduli_za_dodavanje = Modul.objects.filter(skracenica__in = data['moduli']).all()
            for m in moduli_za_dodavanje:
                ispit.moduli.add(m)
            try:
                if (len(smerovi_za_dodavanje)!=0 and len(lokacije_za_dodavanje)!=0 ):
                    ispit.save()
            except:
                error = error + str(ValueError({"ispit": f"Ispit sa podacima naziv:{data['naziv']} rok:{data['rok']} smerovi:{data['smerovi']} moduli:{data['moduli']} nije dobro definisan  !"}))

            print(ispit)
        except:
            error = error + str(ValueError({"ispit": f"Ispit sa podacima naziv:{data['naziv']} rok:{data['rok']} smerovi:{data['smerovi']} moduli:{data['moduli']} nije dobro definisan  !"}))

    if error != "":
        return error

def parseCSV(filePath):
    data_all = csv_loader(filePath)
    error = ""
    for data in data_all:
        try:
            ispit = Ispit()
            ispit.naziv = data[0]
            ispit.godina_studija = data[3]
            ispit.datum = data[6]
            ispit.vreme = data[7]

            rok_za_dodavanje = IspitniRok.objects.get(naziv = data[4])
            ispit.rok = rok_za_dodavanje
            

            smerovi_za_dodavanje = Smer.objects.filter(skracenica__in = data[1]).all()
            for s in smerovi_za_dodavanje:
                ispit.smerovi.add(s)
            
            lokacije_za_dodavanje = Lokacija.objects.filter(skracenica__in = data[5]).all()
            for l in lokacije_za_dodavanje:
                ispit.lokacije.add(l)
            
            moduli_za_dodavanje = Modul.objects.filter(skracenica__in = data[2]).all()
            for m in moduli_za_dodavanje:
                ispit.moduli.add(m)
            
            try:
                if (len(smerovi_za_dodavanje)!=0 and len(lokacije_za_dodavanje)!=0 ):
                    ispit.save()
            except:
                error = error + str(ValueError({"ispit": f"Ispit sa podacima naziv:{data[0]} rok:{data[4]} smerovi:{data[1]} moduli:{data[2]} nije dobro definisan!"}))

            print(ispit)
        except:
            error = error + str(ValueError({"ispit": f"Ispit sa podacima naziv:{data[0]} rok:{data[4]} smerovi:{data[1]} moduli:{data[2]} nije dobro definisan!"}))

    if error != "":
        return error

class Test(APIView):
    permission_classes = [AllowAny]
    def post(self,request):
        error = parseCSV('/home/milan/Desktop/04-Examinator/backend/src/examinator/fixtures/ispiti/sept2/1/test.csv')
        return Response(error)

class KanalPretplateDetail(generics.GenericAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, key):
        if key not in list(map(lambda x: x[0], KanalPretplate.CHANNEL_CHOICES)):
            raise Http404

        try:
            kanal = KanalPretplate.objects.get(user=self.request.user, tip=key)
        except KanalPretplate.DoesNotExist:
            kanal = KanalPretplate(user=self.request.user, tip=key)
            kanal.save()

        aktivan = kanal.aktivan
        return JsonResponse({'aktivan': aktivan})

    def post(self, request, key):
        if key not in list(map(lambda x: x[0], KanalPretplate.CHANNEL_CHOICES)):
            raise Http404
        data = JSONParser().parse(request)
        serializer = KanalAktivanSerializer(data=data)
        serializer.is_valid()
        try:
            kanal = KanalPretplate.objects.get(user=self.request.user, tip=key)
        except KanalPretplate.DoesNotExist:
            kanal = KanalPretplate(user=self.request.user, tip=key)

        kanal.aktivan = serializer.validated_data.get('aktivan')
        kanal.save()
        return JsonResponse(serializer.data, status=200)

class DiscordOdredisteList(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DiscordOdredisteSerializer

    def get_queryset(self):
        kanal_pretplate = KanalPretplate.objects.get(user=self.request.user, tip=KanalPretplate.DISCORD)
        return DiscordOdrediste.objects.filter(kanal_pretplate=kanal_pretplate)

class DiscordOdredisteDetail(generics.RetrieveDestroyAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = DiscordOdredisteSerializer

    def get_queryset(self):
        kanal_pretplate = KanalPretplate.objects.get(user=self.request.user, tip=KanalPretplate.DISCORD)
        return DiscordOdrediste.objects.filter(kanal_pretplate=kanal_pretplate)

class PodrazumevaniFilteriDetails(generics.GenericAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request):
        try:
            filteri = PodrazumevaniFilteri.objects.get(user=self.request.user)
        except PodrazumevaniFilteri.DoesNotExist:
            filteri = PodrazumevaniFilteri(user=self.request.user)
            filteri.save()

        smer_id = filteri.smer_id
        modul_id = filteri.modul_id
        return JsonResponse({'smer_id': smer_id, 'modul_id': modul_id})

    def post(self, request):
        data = JSONParser().parse(request)
        user = self.request.user
        smer = data.get('smer_id')
        modul = data.get('modul_id')
        try:
            prodrazumevani_filteri = PodrazumevaniFilteri.objects.get(user=user)
            prodrazumevani_filteri.smer_id = smer
            prodrazumevani_filteri.modul_id = modul
        except PodrazumevaniFilteri.DoesNotExist:
            prodrazumevani_filteri = PodrazumevaniFilteri(user=user, smer_id=smer, modul_id=modul)

        prodrazumevani_filteri.save()

        return JsonResponse({'smer_id': prodrazumevani_filteri.smer_id, 'modul_id': prodrazumevani_filteri.modul_id})