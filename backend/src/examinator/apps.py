from django.apps import AppConfig


class ExaminatorConfig(AppConfig):
    name = 'src.examinator'
