from celery.utils.log import get_task_logger
from django.core.exceptions import ObjectDoesNotExist

from src.examinator.notification_helpers import send_email, send_discord_message
from src.examinator.models import Ispit, KanalPretplate
from src.celery import app

import sys

logger = get_task_logger(__name__)

@app.task(name="notify_subscribers_via_email_task")
def notify_by_email_task(ispit_id: int, message: str):
    ispit = Ispit.objects.get(pk=ispit_id)
    for sub in ispit.pretplata_set.prefetch_related('user__kanalpretplate_set__emailodrediste_set').all():
        try:
            kanal_pretplate = sub.user.kanalpretplate_set.get(tip=KanalPretplate.EMAIL)
            if not kanal_pretplate.aktivan:
                continue

            odrediste = kanal_pretplate.emailodrediste_set.first()
            if email := odrediste.email:
                send_email(f"Измене испита - {ispit.naziv}", message, email)
        except ObjectDoesNotExist:
            continue


@app.task(name="notify_subscriber_via_discord")
def notify_by_discord_task(ispit_id: int, message: str):
    ispit = Ispit.objects.get(pk=ispit_id)
    sent = 0
    for sub in ispit.pretplata_set.prefetch_related('user__kanalpretplate_set__discordodrediste_set').all():
        try:
            kanal_pretplate = sub.user.kanalpretplate_set.get(tip=KanalPretplate.DISCORD)
            if not kanal_pretplate.aktivan:
                continue

            odrediste = kanal_pretplate.discordodrediste_set.first()
            discord_username = odrediste.discord_username
            discord_discriminator = odrediste.discord_discriminator
            if discord_username and discord_discriminator:
                send_discord_message_task.delay(discord_username, discord_discriminator, message)
                sent += 1
        except ObjectDoesNotExist:
            continue
    return sent

# This task will be put inside a different Queue, as the task limit works only for a single worker
# TODO Add single worker for discord Queue to Celery start script
@app.task(name="send_discord_message_task", rate_limit="30/m", queue="discord")
def send_discord_message_task(discord_username: str, discord_discriminator: str, message: str):
    """Pošalji discord poruku datom korisniku. Korisnik se definiše kao discord_username#discord_discriminator."""
    logger.info(f"Šalje se discord poruka korisniku {discord_username}#{discord_discriminator}")
    send_discord_message(discord_username, discord_discriminator, message)

