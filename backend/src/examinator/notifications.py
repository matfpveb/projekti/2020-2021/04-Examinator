from src.examinator.notification_helpers import generate_email_message

from src.examinator.models import Ispit
from src.examinator.tasks import notify_by_email_task, notify_by_discord_task

def notify_about_changes(ispit: Ispit):
    message = generate_email_message(ispit)
    notify_by_email(ispit.id, message)
    notify_by_discord(ispit.id, message)

def notify_by_email(ispit_id: int, message: str):
    notify_by_email_task.delay(ispit_id, message)

def notify_by_discord(ispit_id: int, message: str):
    notify_by_discord_task.delay(ispit_id, message)
