import discord
import textwrap

from django.core.mail import EmailMessage
from django.conf import settings

from src.examinator.models import Ispit

def send_email(subject: str, message: str, recipients: [str]):
    for recipient in recipients:
        EmailMessage(subject=subject, body=message, to=[recipient]) # TO se ponasa kao CC a ne kao BCC!

def generate_email_message(ispit: Ispit):
    return textwrap.dedent(f"""\
        Поштовани / Поштована,
        
        Дошло је до измена у одржавању испита {ispit.naziv}.
        Време одржавања: {ispit.datum.strftime("%d.%m.%Y.")} у {ispit.vreme}
        Место одржавања: {', '.join([l.skracenica for l in ispit.lokacije.all()])}
        
        Срдачан поздрав,
        Математички факултет
    """)

def send_discord_message(discord_username: str, discord_discriminator: str, message: str):
    class DiscordClient(discord.Client):
        def __init__(self, message, discord_username, discord_discriminator, **options):
            super().__init__(**options)
            self.message = message
            self.dsc_username = discord_username
            self.dsc_discriminator = discord_discriminator

    intents = discord.Intents.all()
    client = DiscordClient(message, discord_username, discord_discriminator, intents=intents)

    @client.event
    async def on_ready():
        find_member = lambda x, n=client.dsc_username, d=client.dsc_discriminator: x.discriminator == d and x.name == n
        user: discord.member = next(iter(filter(find_member, client.get_all_members())), None)
        await user.send(client.message)
        await client.close()

    client.run(settings.DISCORD_BOT_TOKEN)
