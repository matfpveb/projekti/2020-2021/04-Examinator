from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.state import User
from rest_framework_simplejwt.tokens import SlidingToken, RefreshToken

from src.examinator.models import *
from django.contrib.auth import authenticate
from rest_framework.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db import transaction
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer

from src.examinator.notifications import notify_about_changes

class LokacijaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lokacija
        fields = ["id", "skracenica", "lokacija"]


class IspitniRokSerializer(serializers.ModelSerializer):
    class Meta:
        model = IspitniRok
        fields = ["id", "naziv", "pocetak"]


class SmerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Smer
        fields = ["id", "skracenica", "naziv"]


class ModulSerializer(serializers.ModelSerializer):
    class Meta:
        model = Modul
        fields = ["id", "skracenica", "naziv", "smer"]


class IspitSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    naziv = serializers.CharField(max_length=256)
    smerovi = serializers.PrimaryKeyRelatedField(many=True, queryset=Smer.objects.all())
    moduli = serializers.PrimaryKeyRelatedField(many=True, queryset=Modul.objects.all(), required=False)
    godina_studija = serializers.IntegerField(min_value=1, max_value=5)

    rok = serializers.PrimaryKeyRelatedField(queryset=IspitniRok.objects.all())

    lokacije = serializers.PrimaryKeyRelatedField(many=True, queryset=Lokacija.objects.all())
    datum = serializers.DateField()
    vreme = serializers.TimeField()

    def create(self, validated_data):
        with transaction.atomic():
            ispit = Ispit.objects.create(
                naziv=validated_data.get("naziv"),
                godina_studija=validated_data.get("godina_studija"),
                rok=validated_data.get("rok"),
                datum=validated_data.get('datum'),
                vreme=validated_data.get('vreme')
            )

            smerovi = set(validated_data.get('smerovi'))
            for smer in validated_data.get('smerovi'):
                ispit.smerovi.add(smer)

            if (moduli := validated_data.get('moduli')) is not None:
                for modul in moduli:
                    if modul.smer not in smerovi:
                        raise serializers.ValidationError(
                            {"moduli": f"Modul ''{modul.naziv}'' ne pripada ni jednom odabranom smeru!"})
                    ispit.moduli.add(modul)
            for lokacija in validated_data.get('lokacije'):
                ispit.lokacije.add(lokacija)

            return ispit

    def update(self, instance: Ispit, validated_data: dict):

        instance.naziv = validated_data.get('naziv', instance.naziv)

        if smerovi := validated_data.get('smerovi'):
            instance.smerovi.set(smerovi)

        if moduli := validated_data.get('moduli'):
            for modul in moduli:
                if modul.smer not in smerovi:
                    raise serializers.ValidationError(
                        {"moduli": f"Modul ''{modul.naziv}'' ne pripada ni jednom odabranom smeru!"})
            instance.moduli.set(moduli)

        instance.godina_studija = validated_data.get('godina_studija', instance.godina_studija)
        instance.rok = validated_data.get('rok', instance.rok)

        old_lokacije = set(instance.lokacije.values_list('id', flat=True))
        if lokacije := validated_data.get('lokacije'):
            instance.lokacije.set(lokacije)

        old_datum = instance.datum
        old_vreme = instance.vreme

        instance.datum = validated_data.get('datum', instance.datum)
        instance.vreme = validated_data.get('vreme', instance.vreme)

        instance.save()
        new_lokacije = set(instance.lokacije.values_list('id', flat=True))

        if instance.datum != old_datum or instance.vreme != old_vreme or old_lokacije != new_lokacije:
            notify_about_changes(instance)

        return instance

class RegistrationSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirmPassword = serializers.CharField(write_only=True, required=True)

    def validate(self, attrs):
        if attrs['password'] != attrs['confirmPassword']:
            raise serializers.ValidationError({"password": "Lozinke moraju biti iste"})

        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['email'],
            email=validated_data['email'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user

    """Vraćamo token novokreiranog korisnika - registracija je ujedno i login"""
    def to_representation(self, user):
        SlidingToken.for_user(user)
        RefreshToken.for_user(user)

        return {'token': {'access_token': str(RefreshToken.for_user(user).access_token),
                          'refresh_token': str(RefreshToken.for_user(user)), 'name': user.username}}

class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                if not user.is_active:
                    msg = ('User account is disabled.')
                    raise ValidationError(msg)
            else:
                msg = _('Unable to log in with provided credentials.')
                raise ValidationError(msg)
        else:
            msg = _('Must include "username" and "password"')
            raise ValidationError(msg)

        attrs['user'] = user
        return attrs

class CustomJWTSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
            credentials = {
                'username': '',
                'password': attrs.get("password")
            }

            user_obj = User.objects.filter(email=attrs.get("username")).first() \
                       or User.objects.filter(username=attrs.get("username")).first()
            if user_obj:
                credentials['username'] = user_obj.username
            data = super().validate(credentials)

            admin = user_obj.is_staff

            return {'token': {'access_token': data.get('access'), 'refresh_token': data.get('refresh'),
                              'name': user_obj.username, 'is_admin': admin}}


class CustomTokenRefreshSerializer(serializers.Serializer):
    token = serializers.DictField()

    def validate(self, attrs):
        refresh = RefreshToken(attrs.get("token").get("refresh_token"))

        data = {'access': str(refresh.access_token)}

        from rest_framework_simplejwt.settings import api_settings
        if api_settings.ROTATE_REFRESH_TOKENS:
            if api_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = str(refresh)

        return {'token': {'access_token': data.get('access'), 'refresh_token': data.get('refresh'),
                          'name': attrs.get("token").get("name"), 'is_admin': attrs.get("token").get("is_admin")}}

class PretplataSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    ispit = serializers.PrimaryKeyRelatedField(queryset=Ispit.objects.all())
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), default=serializers.CurrentUserDefault())

    def create(self, validated_data):
        user = validated_data.get('user')
        ispit = validated_data.get('ispit')
        try:
            pretplata = Pretplata.objects.get(user=user, ispit=ispit)
        except Pretplata.DoesNotExist:
            pretplata = Pretplata(ispit=ispit, user=user)
            pretplata.save()

        return pretplata

class KanalAktivanSerializer(serializers.Serializer):
    aktivan = serializers.BooleanField()

class DiscordOdredisteSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user = serializers.HiddenField(default=serializers.CurrentUserDefault(), write_only=True)

    discord_username = serializers.CharField(max_length=32)
    discord_discriminator = serializers.CharField(max_length=4)


    def create(self, validated_data):
        kanal_pretplate = KanalPretplate.objects.get(user=validated_data.get('user'), tip=KanalPretplate.DISCORD)
        odrediste = DiscordOdrediste(discord_username=validated_data.get('discord_username'),
                                     discord_discriminator=validated_data.get('discord_discriminator'),
                                     kanal_pretplate=kanal_pretplate)
        odrediste.save()
        return odrediste

