import yaml,csv

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

def yaml_loader(filePath):
    with open(filePath,'r') as f:
        data = yaml.Loader(f).get_data()
    return data
   

def csv_loader(filePath):
    f = open(filePath,'r')
    data = csv.reader(f)

    return data