import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import IspitniRok
from src.examinator.serializers import IspitniRokSerializer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class IspitniRokTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["ispitni_rokovi.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        student = User.objects.create_user(username="student")

        self.tokens = {
            'admin': Token.objects.create(user_id=admin.id),
            'student': Token.objects.create(user_id=student.id)
        }

        self.pks = {
            'valid': IspitniRok.objects.first().id,
            'invalid': IspitniRok.objects.order_by('-id').first().id + 1
        }

        valid_payload = {
            'naziv': 'jan1',
            'pocetak': '2020-03-01'
        }
        invalid_payload = {
            'naziv': '',
            'pocetak': '2020-03-02'
        }

        self.payload = {
            'valid': valid_payload,
            'invalid': invalid_payload
        }


class GetAllIspitniRokoviTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for GET all IspitniRokovi API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_all_ispitni_rokovi_admin(self, user_token):
        response = client.get(reverse('get_post_ispitni_rokovi'),
                              **make_token_header(self.tokens.get(user_token)))

        ispitni_rokovi = IspitniRok.objects.all()
        serializer = IspitniRokSerializer(ispitni_rokovi, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleIspitniRokTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for GET single IspitniRok via API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_ispitni_rok(self, user_token):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pks.get("valid")}),
            **make_token_header(self.tokens.get(user_token)))
        ispitni_rok = IspitniRok.objects.get(pk= self.pks.get("valid"))
        serializer = IspitniRokSerializer(ispitni_rok)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_ispitni_rok_invalid(self, user_token):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pks.get("invalid")}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewIspitniRokTest(IspitniRokTestCaseMixin, TestCase):

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_201_CREATED],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_create_ispitni_rok(self, name, payload, user_token, expected_code):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.payload.get(payload)),
                               content_type='application/json',
                               **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class UpdateSingleIspitniRokTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for updating single IspitniRok via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_200_OK],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_update_single_ispitni_rok(self, name, payload, user_token, expected_code):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pks.get("valid")}),
                              data=json.dumps(self.payload.get(payload)),
                              content_type='application/json',
                              **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class DeleteSingleIspitniRokTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for deleting single IspitniRok via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_204_NO_CONTENT],
        ["invalid_admin", "invalid",  "admin", status.HTTP_404_NOT_FOUND],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_delete_single_ispitni_rok(self, name, pk, user_token, expected_code):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pks.get(pk)}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)
