import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Smer
from src.examinator.serializers import SmerSerializer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class SmerTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["smerovi.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        student = User.objects.create_user(username="student")

        self.tokens = {
            'admin': Token.objects.create(user_id=admin.id),
            'student': Token.objects.create(user_id=student.id)
        }

        self.pks = {
            'valid': Smer.objects.first().id,
            'invalid': Smer.objects.order_by('-id').first().id + 1
        }

        valid_payload = {
            'skracenica': 't10',
            'naziv': 'Test New 10'
        }
        invalid_payload = {
            'skracenica': '',
            'naziv': 'Test 2'
        }

        self.payload = {
            'valid': valid_payload,
            'invalid': invalid_payload
        }


class GetAllSmeroviTest(SmerTestCaseMixin, TestCase):
    """ Test module for GET all Smerovi API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_all_smerovi_admin(self, user_token):
        response = client.get(reverse('get_post_smerovi'),
                              **make_token_header(self.tokens.get(user_token)))

        smerovi = Smer.objects.all()
        serializer = SmerSerializer(smerovi, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleSmerTest(SmerTestCaseMixin, TestCase):
    """ Test module for GET single Smer via API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_smer(self, user_token):
        response = client.get(
            reverse('get_delete_update_smer', kwargs={'pk': self.pks.get("valid")}),
            **make_token_header(self.tokens.get(user_token)))
        smer = Smer.objects.get(pk= self.pks.get("valid"))
        serializer = SmerSerializer(smer)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_smer_invalid(self, user_token):
        response = client.get(
            reverse('get_delete_update_smer', kwargs={'pk': self.pks.get("invalid")}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewSmerTest(SmerTestCaseMixin, TestCase):

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_201_CREATED],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_create_smer(self, name, payload, user_token, expected_code):
        response = client.post(reverse('get_post_smerovi'),
                               data=json.dumps(self.payload.get(payload)),
                               content_type='application/json',
                               **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class UpdateSingleSmerTest(SmerTestCaseMixin, TestCase):
    """ Test module for updating single Smer via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_200_OK],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_update_single_smer(self, name, payload, user_token, expected_code):
        response = client.put(reverse('get_delete_update_smer', kwargs={'pk': self.pks.get("valid")}),
                              data=json.dumps(self.payload.get(payload)),
                              content_type='application/json',
                              **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class DeleteSingleSmerTest(SmerTestCaseMixin, TestCase):
    """ Test module for deleting single Smer via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_204_NO_CONTENT],
        ["invalid_admin", "invalid",  "admin", status.HTTP_404_NOT_FOUND],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_delete_single_smer(self, name, pk, user_token, expected_code):
        response = client.delete(
            reverse('get_delete_update_smer', kwargs={'pk': self.pks.get(pk)}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)
