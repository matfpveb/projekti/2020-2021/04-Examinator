import json

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import IspitniRok
from src.examinator.serializers import IspitniRokSerializer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class IspitniRokoviTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        student = User.objects.create_user(username="student")

        self.token_admin = Token.objects.create(user_id=admin.id)
        self.token_student = Token.objects.create(user_id=student.id)

        self.t1 = IspitniRok.objects.create(
            naziv="jan1", pocetak="2020-03-01")
        self.t2 = IspitniRok.objects.create(
            naziv="jan2", pocetak="2020-03-02")
        self.t3 = IspitniRok.objects.create(
            naziv="jan3", pocetak="2020-03-03")

        self.valid_payload = {
            'naziv': 'jan10',
            'pocetak': '2020-03-01'
        }
        self.invalid_payload = {
            'naziv': '',
            'pocetak': '2020-03-02'
        }


class GetAllIspitniRokoviTest(IspitniRokoviTestCaseMixin, TestCase):
    def test_get_all_ispitni_rokovi_admin(self):
        response = client.get(reverse('get_post_ispitni_rokovi'), **make_token_header(self.token_admin))

        ispitni_rokovi = IspitniRok.objects.all()
        serializer = IspitniRokSerializer(ispitni_rokovi, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_ispitni_rokovi_student(self):
        response = client.get(reverse('get_post_ispitni_rokovi'), **make_token_header(self.token_student))

        ispitni_rokovi = IspitniRok.objects.all()
        serializer = IspitniRokSerializer(ispitni_rokovi, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_all_ispitni_rokovi_no_auth(self):
        response = client.get(reverse('get_post_ispitni_rokovi'))

        ispitni_rokovi = IspitniRok.objects.all()
        serializer = IspitniRokSerializer(ispitni_rokovi, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleIspitniRokTest(IspitniRokoviTestCaseMixin, TestCase):
    """ Test module for GET single IspitniRok via API """

    def test_get_valid_single_ispitni_rok_admin(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk})
            , **make_token_header(self.token_admin))
        ispitni_rok = IspitniRok.objects.get(pk=self.t2.pk)
        serializer = IspitniRokSerializer(ispitni_rok)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_ispitni_rok_admin(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9}),
            **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_valid_single_ispitni_rok_student(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk})
            , **make_token_header(self.token_student))
        ispitni_rok = IspitniRok.objects.get(pk=self.t2.pk)
        serializer = IspitniRokSerializer(ispitni_rok)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_ispitni_rok_student(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9})
            , **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_valid_single_ispitni_rok_no_auth(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk}))
        ispitni_rok = IspitniRok.objects.get(pk=self.t2.pk)
        serializer = IspitniRokSerializer(ispitni_rok)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_ispitni_rok_no_auth(self):
        response = client.get(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewIspitniRokTest(IspitniRokoviTestCaseMixin, TestCase):

    def test_create_valid_ispitni_rok_admin(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.valid_payload),
                               content_type='application/json',
                               **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_ispitni_rok_admin(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.invalid_payload),
                               content_type='application/json',
                               **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_valid_ispitni_rok_student(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.valid_payload),
                               content_type='application/json',
                               **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_invalid_ispitni_rok_student(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.invalid_payload),
                               content_type='application/json',
                               **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_valid_ispitni_rok_no_auth(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.valid_payload),
                               content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_create_invalid_ispitni_rok_no_auth(self):
        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(self.invalid_payload),
                               content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class UpdateSingleIspitniRokTest(IspitniRokoviTestCaseMixin, TestCase):
    """ Test module for updating single IspitniRok via API """

    def test_update_valid_single_ispitni_rok_admin(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.valid_payload),
                              content_type='application/json',
                              **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_invalid_single_ispitni_rok_admin(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.invalid_payload),
                              content_type='application/json',
                              **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_valid_single_ispitni_rok_student(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.valid_payload),
                              content_type='application/json',
                              **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_invalid_single_ispitni_rok_student(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.invalid_payload),
                              content_type='application/json',
                              **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_valid_single_ispitni_rok_no_auth(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.valid_payload),
                              content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_invalid_single_ispitni_rok_no_auth(self):
        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t1.pk}),
                              data=json.dumps(self.invalid_payload),
                              content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class DeleteSingleIspitniRokTest(IspitniRokoviTestCaseMixin, TestCase):
    """ Test module for deleting single IspitniRok via API """

    def test_delete_valid_single_ispitni_rok_admin(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk}), **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_invalid_single_ispitni_rok_admin(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9}), **make_token_header(self.token_admin))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_valid_single_ispitni_rok_student(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk}),
            **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_invalid_single_ispitni_rok_student(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9}), **make_token_header(self.token_student))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_valid_single_ispitni_rok_no_auth(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.t2.pk}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_invalid_single_ispitni_rok_no_auth(self):
        response = client.delete(
            reverse('get_delete_update_ispitni_rok', kwargs={'pk': 9}))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
