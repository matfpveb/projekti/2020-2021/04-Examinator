import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Ispit, IspitniRok
from src.examinator.serializers import IspitSerializer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class IspitTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["smerovi.yaml", "moduli.yaml", "lokacije.yaml", "ispitni_rokovi.yaml"]
    fixtures += ["ispiti/jan1/1/m.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        student = User.objects.create_user(username="student")

        self.tokens = {
            'admin': Token.objects.create(user_id=admin.id),
            'student': Token.objects.create(user_id=student.id)
        }

        self.pks = {
            'valid': Ispit.objects.first().id,
            'invalid': Ispit.objects.order_by('-id').first().id + 1
        }

        self.rok_pk = {
            'valid': IspitniRok.objects.first().id,
            'invalid': IspitniRok.objects.order_by('-id').first().id + 1
        }

        valid_payload = {
            'naziv': 'Основи механике',
            'smerovi': [2],
            'moduli': [],
            'godina_studija': 2,
            'rok': 1,
            'lokacije': [1],
            'datum': '2021-01-23',
            'vreme': '09:00:00',
        }
        invalid_payload = {
            'naziv': 'Основи механике',
            'smerovi': [2],
            'moduli': [],
            'godina_studija': 'lose',
            'rok': 1,
            'lokacije': [1],
            'datum': '2021-01-23',
            'vreme': '09:00:00',
        }

        self.payload = {
            'valid': valid_payload,
            'invalid': invalid_payload
        }


class GetAllIspitiTest(IspitTestCaseMixin, TestCase):
    """ Test module for GET all Ispiti API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_all_ispiti_admin(self, user_token):
        response = client.get(reverse('get_post_ispiti_by_rok', kwargs={'rok_id': self.rok_pk.get("valid")}),
                              **make_token_header(self.tokens.get(user_token)))

        ispiti = Ispit.objects.all()
        serializer = IspitSerializer(ispiti, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleIspitTest(IspitTestCaseMixin, TestCase):
    """ Test module for GET single Ispit via API """

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_ispit(self, user_token):
        response = client.get(
            reverse('get_delete_update_ispit_by_rok', kwargs={'rok_id': self.rok_pk.get("valid"), 'pk': self.pks.get("valid")}),
            **make_token_header(self.tokens.get(user_token)))
        ispit = Ispit.objects.get(pk= self.pks.get("valid"))
        serializer = IspitSerializer(ispit)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @parameterized.expand(["admin", "student", "no_auth"])
    def test_get_single_ispit_invalid(self, user_token):
        response = client.get(
            reverse('get_delete_update_ispit_by_rok', kwargs={'rok_id': self.rok_pk.get("valid"),'pk': self.pks.get("invalid")}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewIspitTest(IspitTestCaseMixin, TestCase):

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_201_CREATED],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_create_ispit(self, name, payload, user_token, expected_code):
        response = client.post(reverse('get_post_ispiti_by_rok', kwargs={'rok_id': self.rok_pk.get("valid")}),
                               data=json.dumps(self.payload.get(payload)),
                               content_type='application/json',
                               **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class UpdateSingleIspitTest(IspitTestCaseMixin, TestCase):
    """ Test module for updating single Ispit via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_200_OK],
        ["invalid_admin", "invalid",  "admin", status.HTTP_400_BAD_REQUEST],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_update_single_ispit(self, name, payload, user_token, expected_code):
        response = client.put(reverse('get_delete_update_ispit_by_rok', kwargs={'rok_id': self.rok_pk.get("valid"),'pk': self.pks.get("valid")}),
                              data=json.dumps(self.payload.get(payload)),
                              content_type='application/json',
                              **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)


class DeleteSingleIspitTest(IspitTestCaseMixin, TestCase):
    """ Test module for deleting single Ispit via API """

    @parameterized.expand([
        ["valid_admin", "valid", "admin", status.HTTP_204_NO_CONTENT],
        ["invalid_admin", "invalid",  "admin", status.HTTP_404_NOT_FOUND],
        ["valid_student", "valid",  "student", status.HTTP_403_FORBIDDEN],
        ["invalid_student", "invalid", "student", status.HTTP_403_FORBIDDEN],
        ["valid_no_auth", "valid", "no_auth", status.HTTP_401_UNAUTHORIZED],
        ["invalid_no_auth", "invalid", "no_auth", status.HTTP_401_UNAUTHORIZED],
    ])
    def test_delete_single_ispit(self, name, pk, user_token, expected_code):
        response = client.delete(
            reverse('get_delete_update_ispit_by_rok', kwargs={'rok_id': self.rok_pk.get("valid"),'pk': self.pks.get(pk)}),
            **make_token_header(self.tokens.get(user_token)))
        self.assertEqual(response.status_code, expected_code)
