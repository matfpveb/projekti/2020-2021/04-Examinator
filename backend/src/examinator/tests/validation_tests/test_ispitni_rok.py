import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import IspitniRok

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class IspitniRokTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["ispitni_rokovi.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        self.token = Token.objects.create(user_id=admin.id)
        self.pk = IspitniRok.objects.first().id
        self.valid_naziv = "jun 1"
        self.valid_pocetak = "2020-03-04"


class CreateNewIspitniRokTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for checking data validity of create IspitniRok on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_256", "t"*257]
    ])
    def test_create_ispitni_rok_bad_naziv(self, name, naziv):
        payload = {
            'pocetak': self.valid_pocetak
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.post(reverse('get_post_ispitni_rokovi'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["wrong_format", "04.04.2021."],
        ["incomplete", "03.03."],
        ["nonsense_month", "2021-13-01"],
        ["nonsense_day", "2021-12-32"],
        ["february", "2021-02-29"]
    ])
    def test_update_ispitni_rok_bad_pocetak(self, name, pocetak):
        payload = {
            'naziv': self.valid_naziv
        }

        if pocetak is not None:
            payload['pocetak'] = pocetak

        response = client.post(reverse('get_post_ispitni_rokovi'),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleIspitniRokTest(IspitniRokTestCaseMixin, TestCase):
    """ Test module for checking data validity of single IspitniRok on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_256", "t"*257]
    ])
    def test_update_ispitni_rok_bad_naziv(self, name, naziv):
        payload = {
            'pocetak': self.valid_pocetak
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["wrong_format", "04.04.2021."],
        ["incomplete", "03.03."],
        ["nonsense_month", "2021-13-01"],
        ["nonsense_day", "2021-12-32"],
        ["february", "2021-02-29"]
    ])
    def test_update_ispitni_rok_bad_pocetak(self, name, pocetak):
        payload = {
            'naziv': self.valid_naziv
        }

        if pocetak is not None:
            payload['pocetak'] = pocetak

        response = client.put(reverse('get_delete_update_ispitni_rok', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
