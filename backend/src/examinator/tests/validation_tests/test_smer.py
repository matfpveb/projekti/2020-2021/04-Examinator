import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Smer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class SmerTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["smerovi.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        self.token = Token.objects.create(user_id=admin.id)
        self.pk = Smer.objects.first().id
        self.valid_skracenica = "T1"
        self.valid_naziv = "Test"


class CreateNewSmerTest(SmerTestCaseMixin, TestCase):
    """ Test module for checking data validity of create Smer on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_create_smer_bad_skracenica(self, name, skracenica):
        payload = {
            'naziv': self.valid_naziv
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.post(reverse('get_post_smerovi'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_64", "t"*65]
    ])
    def test_create_smer_bad_naziv(self, name, naziv):
        payload = {
            'skracenica': self.valid_skracenica
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.post(reverse('get_post_smerovi'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleSmerTest(SmerTestCaseMixin, TestCase):
    """ Test module for checking data validity of single Smer on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_update_smer_bad_skracenica(self, name, skracenica):
        payload = {
            'naziv': self.valid_naziv
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.put(reverse('get_delete_update_smer', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_64", "t"*65]
    ])
    def test_update_smer_bad_naziv(self, name, naziv):
        payload = {
            'skracenica': self.valid_skracenica
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.put(reverse('get_delete_update_smer', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)