import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Lokacija

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class LokacijaTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["lokacije.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        self.token = Token.objects.create(user_id=admin.id)
        self.pk = Lokacija.objects.first().id
        self.valid_skracenica = "T1"
        self.valid_lokacija = "Test"


class CreateNewLokacijaTest(LokacijaTestCaseMixin, TestCase):
    """ Test module for checking data validity of create Lokacija on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_create_lokacija_bad_skracenica(self, name, skracenica):
        payload = {
            'lokacija': self.valid_lokacija
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.post(reverse('get_post_lokacije'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_128", "t"*129]
    ])
    def test_create_lokacija_bad_lokacija(self, name, lokacija):
        payload = {
            'skracenica': self.valid_skracenica
        }

        if lokacija is not None:
            payload['lokacija'] = lokacija

        response = client.post(reverse('get_post_lokacije'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleLokacijaTest(LokacijaTestCaseMixin, TestCase):
    """ Test module for checking data validity of single Lokacija on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_update_lokacija_bad_skracenica(self, name, skracenica):
        payload = {
            'lokacija': self.valid_lokacija
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.put(reverse('get_delete_update_lokacija', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_128", "t"*129]
    ])
    def test_update_lokacija_bad_lokacija(self, name, lokacija):
        payload = {
            'skracenica': self.valid_skracenica
        }

        if lokacija is not None:
            payload['lokacija'] = lokacija

        response = client.put(reverse('get_delete_update_lokacija', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)