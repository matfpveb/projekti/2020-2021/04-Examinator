import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Modul, Smer

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class ModulTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["smerovi.yaml", "moduli.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        self.token = Token.objects.create(user_id=admin.id)
        self.pk = Modul.objects.first().id
        self.valid_skracenica = "T1"
        self.valid_naziv = "Test"
        self.valid_smer_id = Smer.objects.first().id


class CreateNewModulTest(ModulTestCaseMixin, TestCase):
    """ Test module for checking data validity of create Modul on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_create_modul_bad_skracenica(self, name, skracenica):
        payload = {
            'naziv': self.valid_naziv,
            'smer_id': self.valid_smer_id
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.post(reverse('get_post_moduli'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_128", "t"*129]
    ])
    def test_create_modul_bad_naziv(self, name, naziv):
        payload = {
            'skracenica': self.valid_skracenica,
            'smer_id': self.valid_smer_id
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.post(reverse('get_post_moduli'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", -1],
        ["zero", 0],
        ["list", [1, 2]]
    ])
    def test_create_modul_bad_smer(self, name, smer_id):
        payload = {
            'skracenica': self.valid_skracenica,
            'naziv': self.valid_naziv
        }

        if smer_id is not None:
            payload['smer'] = smer_id

        response = client.post(reverse('get_post_moduli'),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class UpdateSingleModulTest(ModulTestCaseMixin, TestCase):
    """ Test module for checking data validity of single Modul on update via API """

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_8", "123456789"]
    ])
    def test_update_modul_bad_skracenica(self, name, skracenica):
        payload = {
            'naziv': self.valid_naziv,
            'smer_id': self.valid_smer_id
        }

        if skracenica is not None:
            payload['skracenica'] = skracenica

        response = client.put(reverse('get_delete_update_modul', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_128", "t"*129]
    ])
    def test_update_modul_bad_naziv(self, name, naziv):
        payload = {
            'skracenica': self.valid_skracenica,
            'smer_id': self.valid_smer_id
        }

        if naziv is not None:
            payload['naziv'] = naziv
        response = client.put(reverse('get_delete_update_modul', kwargs={'pk': self.pk}),
                              data=json.dumps(payload),
                              content_type='application/json',
                              **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", -1],
        ["zero", 0],
        ["list", [1, 2]]
    ])
    def test_update_modul_bad_smer(self, name, smer_id):
        payload = {
            'skracenica': self.valid_skracenica,
            'naziv': self.valid_naziv
        }

        if smer_id is not None:
            payload['smer'] = smer_id

        response = client.put(reverse('get_delete_update_modul', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)