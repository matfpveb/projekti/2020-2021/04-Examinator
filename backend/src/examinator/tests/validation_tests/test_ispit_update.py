import json
from parameterized import parameterized

from rest_framework import status
from rest_framework.authtoken.models import Token

from django.test import TestCase, Client
from django.test.testcases import SerializeMixin

from django.urls import reverse
from django.contrib.auth.models import User

from src.examinator.models import Ispit

from src.examinator.tests.helpers.helpers import make_token_header


client = Client()


class IspitTestCaseMixin(SerializeMixin):
    """
    @DynamicAttrs
    """
    lockfile = __file__
    fixtures = ["smerovi.yaml", "moduli.yaml", "lokacije.yaml", "ispitni_rokovi.yaml"]
    fixtures += ["ispiti/jan1/1/m.yaml"]

    def setUp(self):
        admin = User.objects.create_superuser(username="admin_test")
        self.token = Token.objects.create(user_id=admin.id)
        self.pk = Ispit.objects.first().id
        self.valid_naziv = "Programiranje za veb"
        self.valid_smerovi = [1]
        self.valid_moduli = [1]
        self.valid_godina_studija = 3

        self.valid_rok = 1

        self.valid_lokacije = [1]
        self.valid_vreme = "16:00:00"
        self.valid_datum = "2020-03-04"


class UpdateSingleIspitTest(IspitTestCaseMixin, TestCase):
    """ Test module for checking data validity of single Ispit on update via API """


    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["longer_than_256", "t"*258]
    ])
    def test_create_ispit_bad_naziv(self, name, naziv):
        payload = {
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if naziv is not None:
            payload['naziv'] = naziv

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", [-1]],
        ["zero", [0]],
        ["non-existant", [999999]]
    ])
    def test_create_ispit_bad_smerovi(self, name, smerovi):
        payload = {
            'naziv': self.valid_naziv,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if smerovi is not None:
            payload['smerovi'] = smerovi

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["negative", [-1]],
        ["zero", [0]],
        ["non-existant", [999999]]
    ])
    def test_create_ispit_bad_moduli(self, name, moduli):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if moduli is not None:
            payload['moduli'] = moduli

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_ispit_bad_moduli_smer(self):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': [2],      # Informatics doesn't have any modules
            'moduli': [1, 2],    # Modules 1&2 belong to Mathematics
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", -1],
        ["zero", 0],
        ["larger-than-allowd", 6],
        ["very_big", 999999],
        ['list', [1, 2]]
    ])
    def test_create_ispit_bad_godina_studija(self, name, godina_studija):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'rok': self.valid_rok,
            'lokacije': self.valid_lokacije,

            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if godina_studija is not None:
            payload['godina_studija'] = godina_studija

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", -1],
        ["zero", 0],
        ["non-existant", 999999],
        ['list', [1, 2]]
    ])
    def test_create_ispit_bad_rok(self, name, rok):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'lokacije': self.valid_lokacije,

            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if rok is not None:
            payload['rok'] = rok

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["negative", [-1]],
        ["zero", [0]],
        ["non-existant", [999999]]
    ])
    def test_create_ispit_bad_lokacije(self, name, lokacije):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'datum': self.valid_datum,
            'vreme': self.valid_vreme
        }

        if lokacije is not None:
            payload['lokacije'] = lokacije

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["wrong_format", "04.04.2021."],
        ["incomplete", "03.03."],
        ["nonsense_month", "2021-13-01"],
        ["nonsense_day", "2021-12-32"],
        ["february", "2021-02-29"]
    ])
    def test_update_ispit_bad_datum(self, name, datum):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'vreme': self.valid_vreme
        }

        if datum is not None:
            payload['datum'] = datum

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @parameterized.expand([
        ["empty", ""],
        ["none", None],
        ["wrong_format", "13.00.00"],
        ["nonsense_hour", "25:00:00"],
        ["nonsense_minute", "13:61:00"],
        ["nonsense_second", "13:00:61"]
    ])
    def test_update_ispit_bad_vreme(self, name, vreme):
        payload = {
            'naziv': self.valid_naziv,
            'smerovi': self.valid_smerovi,
            'moduli': self.valid_moduli,
            'godina_studija': self.valid_godina_studija,
            'rok': self.valid_rok,

            'lokacije': self.valid_lokacije,
            'datum': self.valid_datum
        }

        if vreme is not None:
            payload['vreme'] = vreme

        response = client.put(reverse('get_delete_update_ispit', kwargs={'pk': self.pk}),
                               data=json.dumps(payload),
                               content_type='application/json',
                               **make_token_header(self.token))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

