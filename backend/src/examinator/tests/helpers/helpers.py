from rest_framework.authtoken.models import Token

def make_token_header(token: Token = None):
    if token is not None:
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Token ' + str(token),
        }
    else:
        auth_headers = {}

    return auth_headers
