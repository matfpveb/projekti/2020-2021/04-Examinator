"""src URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.http import JsonResponse
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_simplejwt import views as jwt_views
from src.examinator import views
from src.examinator.serializers import CustomJWTSerializer, CustomTokenRefreshSerializer
from src.examinator.views import DiscordLinkView, Test
from src.examinator.serializers import CustomJWTSerializer, CustomTokenRefreshSerializer, KanalPretplate
from src.examinator.views import DiscordLinkView
from src.settings import DISCORD_INVITE_LINK

api_v1 = 'api/v1/';

urlpatterns = [
    path('admin/', admin.site.urls),
    path(api_v1 + 'lokacije/', views.LokacijaList.as_view(), name="get_post_lokacije"),
    path(api_v1 + 'lokacije/<int:pk>/', views.LokacijaDetail.as_view(), name="get_delete_update_lokacija"),
    path(api_v1 + 'ispitni_rokovi/', views.IspitniRokList.as_view(), name="get_post_ispitni_rokovi"),
    path(api_v1 + 'ispitni_rokovi/<int:pk>/', views.IspitniRokDetail.as_view(), name="get_delete_update_ispitni_rok"),
    path(api_v1 + 'smerovi/', views.SmerList.as_view(), name="get_post_smerovi"),
    path(api_v1 + 'smerovi/<int:pk>/', views.SmerDetail.as_view(), name="get_delete_update_smer"),
    path(api_v1 + 'moduli/', views.ModulList.as_view(), name="get_post_moduli"),
    path(api_v1 + 'moduli/<int:pk>/', views.ModulDetail.as_view(), name="get_delete_update_modul"),

    path(api_v1 + 'login/', jwt_views.TokenObtainPairView.as_view(serializer_class=CustomJWTSerializer), name='token_obtain_pair'),
    path(api_v1 + 'token/refresh/', jwt_views.TokenRefreshView.as_view(serializer_class=CustomTokenRefreshSerializer), name='token_refresh'),
    path(api_v1 + 'logout/', views.LogoutView.as_view(), name='logout'),
    path(api_v1 + 'register/', views.RegisterView.as_view(), name='register'),

    path(api_v1 + 'ispiti/', views.IspitList.as_view(), name="get_post_ispiti"),
    path(api_v1 + 'ispiti/<int:pk>/', views.IspitDetail.as_view(), name="get_delete_update_ispit"),
    path(api_v1 + 'ispitni_rokovi/<int:rok_id>/ispiti/', views.IspitByRokList.as_view(), name="get_post_ispiti_by_rok"),
    path(api_v1 + 'ispitni_rokovi/<int:rok_id>/ispiti/<int:pk>/',
         views.IspitByRokDetail.as_view(), name="get_delete_update_ispit_by_rok"),

    path('api/v1/discord_link/', DiscordLinkView.as_view(), name="discord_link"),
    path('test/', Test.as_view(), name="test"),
    path('api/v1/korisnik/promena_lozinke/', views.ChangePasswordView.as_view(), name="change_password"),
    path('api/v1/korisnik/pretplate/', views.PretplataViewList.as_view(), name="get_post_pretplata"),
    path('api/v1/korisnik/pretplate/<int:pk>/', views.PretplataViewDetail.as_view(), name="get_delete_pretplata"),
    path(api_v1 + 'discord_link/', DiscordLinkView.as_view(), name="discord_link"),

    path(api_v1 + 'korisnik/promena_lozinke/', views.ChangePasswordView.as_view(), name="change_password"),
    path(api_v1 + 'korisnik/pretplate/', views.PretplataViewList.as_view(), name="get_post_pretplata"),
    path(api_v1 + 'korisnik/pretplate/<int:pk>/', views.PretplataViewDetail.as_view(), name="get_delete_pretplata"),

    path(api_v1 + 'korisnik/kanali_pretplate/<str:key>/', views.KanalPretplateDetail.as_view(), name="get_post_kanali"),
    path(api_v1 + 'korisnik/kanali_pretplate/' + KanalPretplate.DISCORD +'/odredista/',
         views.DiscordOdredisteList.as_view(), name="get_post_discord_odrediste"),
    path(api_v1 + 'korisnik/kanali_pretplate/' + KanalPretplate.DISCORD +'/odredista/<int:pk>/',
         views.DiscordOdredisteDetail.as_view(), name="get_delete_discord_odrediste"),

    path(api_v1 + 'korisnik/podrazumevani_filteri/', views.PodrazumevaniFilteriDetails.as_view(),
         name='get_post_podrazumevani_filteri'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
